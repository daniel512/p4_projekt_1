CREATE TABLE Contractors (
	ContractorId Int Identity(1, 1),
	ContractorCreateDate DateTime,
	ShortName varchar(64),
	FullName varchar(255),
	Country varchar(64),
	City varchar(64),
	NIP varchar(24),
	PRIMARY KEY (ContractorId)
)


CREATE TABLE Documents (
	DocumentId Int Identity(1, 1),
	DocumentCreateDate DateTime,
	DocumentNumber varchar(64),
	ContractorId int,
	Descript varchar(255),
	Sale float,
	OtherIncomes float,
	TotalIncomes float,
	Purchase float,
	PurchaseCost float,
	Salary float,
	OtherOutcomes float,
	TotalOutcomes float,
	PRIMARY KEY (DocumentId),
	FOREIGN KEY (ContractorId) REFERENCES Contractors(ContractorId)
)


INSERT INTO Contractors VALUES 
	(
		GETDATE(),
		'Kołdrex POL',
		'Kołdrex POL sp. z o.o.',
		'Polska',
		'Warszawa',
		'554-221-77-32'
	),
	(
		GETDATE(),
		'Budmax',
		'Budmax POL sp. z o.o.',
		'Polska',
		'Katowice',
		'734-211-36-44'
	)

INSERT INTO Documents VALUES 
	(
		GETDATE(),
		'FS 1/2020',
		1,
		'Sprzedaż towarów',
		1000,
		120,
		1120,
		0,
		0,
		0,
		0,
		0
	),	
	(
		GETDATE(),
		'FZ 1/2020',
		2,
		'Zakup Materiałów',
		0,
		0,
		0,
		500,
		0,
		0,
		0,
		0
	)

