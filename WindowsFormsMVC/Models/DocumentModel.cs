﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsMVC.Models
{
    public class DocumentModel
    {
        public int DocumentId { get; set; }
        public string DocumentCreateDate { get; set; }
        public string DocumentNumber { get; set; }
        public int CotractorId { get; set; }
        public string Descript { get; set; }
        public double Sale { get; set; }
        public double OtherIncomes { get; set; }
        public double TotalIncomes { get; set; }
        public double Purchase { get; set; }
        public double PurchaseCost { get; set; }
        public double Salary { get; set; }
        public double OtherOutcomes { get; set; }
        public double TotalOutcomes { get; set; }
        //CONTACTORS PROPERTIES
        public string ShortName { get; set; }
    }
}
