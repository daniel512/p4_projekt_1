﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsMVC.Models
{
    public class ContractorModel
    {
        public int ContractorId { get; set; }
        public string ContractorCreateDate { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string NIP { get; set; }
    }
}
