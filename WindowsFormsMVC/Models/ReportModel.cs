﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsMVC.Models
{
    public class ReportModel
    {
        public double SaleSum { get; set; }
        public double OtherIncomesSum { get; set; }
        public double TotalIncomesSum { get; set; }
        public double PurchaseSum { get; set; }
        public double PurchaseCostSum { get; set; }
        public double SalarySum { get; set; }
        public double OtherOutcomesSum { get; set; }
        public double TotalOutcomesSum { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
    }
}
