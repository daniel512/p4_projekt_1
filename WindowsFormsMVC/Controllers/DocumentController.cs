﻿using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsMVC.Interfaces;
using WindowsFormsMVC.Models;

namespace WindowsFormsMVC.Controllers
{
    public class DocumentController
    {
        IList _documents;
        public IList Contractors { get; set; }
        IDocumentView _view;

        public DocumentController(IDocumentView view, IList documents)
        {
            _documents = documents;
            _view = view;
            view.SetController(this);
        }
        /*
         * metoda wypełnia textBoxy wartościami modelu,
         * gettery zdefiniowane w klasie widoku
         */    
        public void UpdateViewDetailValues(DocumentModel document)
        {
            _view.DocumentNumber = document.DocumentNumber;
            _view.Descript       = document.Descript;
            _view.Sale           = document.Sale;
            _view.OtherIncomes   = document.OtherIncomes;
            _view.TotalIncomes   = document.TotalIncomes;
            _view.Purchase       = document.Purchase;
            _view.PurchaseCost   = document.PurchaseCost;
            _view.Salary         = document.Salary;
            _view.OtherOutcomes  = document.OtherOutcomes;
            _view.TotalOutcomes  = document.TotalOutcomes;
        }
        public void UpdateModelWithViewValues(DocumentModel document)
        {
            document.DocumentNumber = _view.DocumentNumber;
            document.Descript       = _view.Descript;
            document.ShortName      = _view.ShortName;
            document.Sale           = _view.Sale;
            document.OtherIncomes   = _view.OtherIncomes;
            document.TotalIncomes   = _view.TotalIncomes;
            document.Purchase       = _view.Purchase;
            document.PurchaseCost   = _view.PurchaseCost;
            document.Salary         = _view.Salary;
            document.OtherOutcomes  = _view.OtherOutcomes;
            document.TotalOutcomes  = _view.TotalOutcomes;
        }
        public void SelectedDocumentChanged(string selectedDocumentId)
        {
            foreach (DocumentModel document in _documents)
            {
                if (document.DocumentId.ToString() == selectedDocumentId)
                {
                    UpdateViewDetailValues(document);
                    _view.SetSelectedDocumentInGrid(document);
                    break;
                }
            }
        }
        /*
         * Metoda wywoływana po każdym zapytaniu do bazy,
         * zapewnia widokowi aktualne dane 
         */
        public void UpdateDocumentsList()
        {
            string sql = "SELECT * FROM Documents d " +
                         "INNER JOIN Contractors c ON c.ContractorId = d.ContractorId";
            using (var conn = DbConnection.GetInstance().Connect())
            {
                _documents = conn.Query<DocumentModel>(sql).ToList();
            }
        }
        /*
         * Metoda wykorzystywana do sprawdzenia, czy lista 
         * kontrahentów nie jest pusta (każdy dokument 
         * musi mieć przypisanego kontrahenta)
         */
        public void CheckContractors()
        {
            string sql = "SELECT * FROM Contractors";
            using (var conn = DbConnection.GetInstance().Connect())
            {
                Contractors = conn.Query<ContractorModel>(sql).ToList();          
            }          
        }
        /* Metoda stanowi źródło danych dla comboBoxa */
        public List<string> GetContractorsNames()
        {
            List<string> names = new List<string>();
            foreach (ContractorModel contractor in Contractors)
            {
                names.Add(contractor.ShortName);
            }
            return names;
        }
        public void LoadView()
        {
            _view.ClearGrid();
            foreach (DocumentModel document in _documents)
            {
                _view.AddDocumentToGrid(document);
            }
        }
        public void AddNewDocument()
        {
            DocumentModel document = new DocumentModel();
            UpdateModelWithViewValues(document);
            this.InsertDocument(document);
            this.UpdateDocumentsList();
            this.LoadView();
        }
        public void InsertDocument(DocumentModel document)
        {
            int contractorId;
            string sql_1 = $"SELECT ContractorId From Contractors " +
                           $"WHERE ShortName LIKE '{document.ShortName}'";

            using(var conn = DbConnection.GetInstance().Connect())
            {
                contractorId = conn.Query<int>(sql_1).Single();
            }

            string sql_2 =   $"INSERT INTO Documents VALUES(" +
                             $"GETDATE(), " +
                             $"'{document.DocumentNumber}', " +
                             $"{contractorId}, " +
                             $"'{document.Descript}', " +
                             $"{document.Sale}, " +
                             $"{document.OtherIncomes}, " +
                             $"{document.TotalIncomes}, " +
                             $"{document.Purchase}, " +
                             $"{document.PurchaseCost}, " +
                             $"{document.Salary}, " +
                             $"{document.OtherIncomes}, " +
                             $"{document.TotalOutcomes}" +
                             $")";

            using (var conn = DbConnection.GetInstance().Connect())
            {
                conn.Execute(sql_2);
            }
        }
        public void UpdateDocument(int documentId)
        {
            DocumentModel document = new DocumentModel();
            UpdateModelWithViewValues(document);
            string sql =  $"UPDATE Documents SET " +
                          $"DocumentNumber   = '{document.DocumentNumber}', " +
                          $"Descript         = '{document.Descript}', " +
                          $"Sale             = {document.Sale}, " +
                          $"OtherIncomes     = {document.OtherIncomes}, " +
                          $"TotalIncomes     = {document.TotalIncomes}, "+
                          $"Purchase         = {document.Purchase}, " +
                          $"PurchaseCost     = {document.PurchaseCost}, " +
                          $"Salary           = {document.Salary}, " +
                          $"OtherOutcomes    = {document.OtherOutcomes}, " +
                          $"TotalOutcomes    = {document.TotalOutcomes} " +
                          $"WHERE DocumentId = {documentId}";

            using (var conn = DbConnection.GetInstance().Connect())
            {
               conn.Execute(sql);
            }
            this.UpdateDocumentsList();
            this.LoadView();
        }
        public void DeleteDocument(int documentId)
        {
            string sql = $"DELETE FROM Documents " +
                         $"WHERE DocumentId = {documentId}";

            using (var conn = DbConnection.GetInstance().Connect())
            {
                conn.Execute(sql);
            }
            this.UpdateDocumentsList();
            this.LoadView();
        }
    }
}
