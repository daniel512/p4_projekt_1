﻿using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsMVC.Interfaces;
using WindowsFormsMVC.Models;

namespace WindowsFormsMVC.Controllers
{
    public class ContractorController
    {
        IContractorView _view;
        IList _contractors;
        public ContractorController(IContractorView view, IList contractors)
        {
            _view           = view;
            _contractors    = contractors;
            _view.SetController(this);
        }
        /*
         * Metoda wypełnia textBoxy wartościami modelu, 
         * gettery zdefiniowane w klasie widoku
         */
        public void UpdateViewDetailValues(ContractorModel contractor)
        {
            _view.ShortName     = contractor.ShortName;
            _view.FullName      = contractor.FullName;
            _view.Country       = contractor.Country;
            _view.City          = contractor.City;
            _view.NIP           = contractor.NIP;
        }
        public void UpdateModelWithViewValues(ContractorModel contractor)
        {
            contractor.ShortName    = _view.ShortName;
            contractor.FullName     = _view.FullName;
            contractor.Country      = _view.Country;
            contractor.City         = _view.City;
            contractor.NIP          = _view.NIP;
        }
        /* 
         * Metoda przypisuje modelowi wartości
         * wybranego przedmiotu w kontrolce listView 
         */
        public void SelectedContractorChanged(string selectedContractorId)
        {
            foreach (ContractorModel contractor in _contractors)
            {
                if (contractor.ContractorId.ToString() == selectedContractorId)
                {
                    UpdateViewDetailValues(contractor);
                    _view.SetSelectedContractorInGrid(contractor);
                    break;
                }
            }
        }
        /*
         * Metoda wywoływana po każdym zapytaniu do bazy,
         * zapewnia widokowi aktualne dane 
         */
        public void UpdateContractorsList()
        {
            string sql = "SELECT * FROM Contractors";
            using (var conn = DbConnection.GetInstance().Connect())
            {
                _contractors = conn.Query<ContractorModel>(sql).ToList();
            }
        }
        public void AddNewContractor()
        {
            ContractorModel contractor = new ContractorModel();
            UpdateModelWithViewValues(contractor);
            this.InsertContractor(contractor);
            this.UpdateContractorsList();
            this.LoadView();
        }
        public void LoadView()
        {
            _view.ClearGrid();
            foreach (ContractorModel contractor in _contractors)
            {
                _view.AddContractorToGrid(contractor);
            }
        }
        public void InsertContractor(ContractorModel contractor)
        {
            string sql = $"INSERT INTO Contractors VALUES(" +
                         $"GETDATE(), " +
                         $"'{contractor.ShortName}', " +
                         $"'{contractor.FullName}', " +
                         $"'{contractor.Country}', " +
                         $"'{contractor.City}', " +
                         $"'{contractor.NIP}'" +
                         $")";

            using (var conn = DbConnection.GetInstance().Connect())
            {
                conn.Execute(sql);
            }
        }
        public void UpdateContractor(int contractorId)
        {
            ContractorModel contractor = new ContractorModel();
            UpdateModelWithViewValues(contractor);
            string sql =  $"UPDATE Contractors SET " +
                          $"ShortName          = '{contractor.ShortName}', " +
                          $"FullName           = '{contractor.FullName}', " +
                          $"Country            = '{contractor.Country}', " +
                          $"City               = '{contractor.City}' " +
                          $"WHERE ContractorId =  {contractorId}";

            using (var conn = DbConnection.GetInstance().Connect())
            {
                conn.Execute(sql);
            }
            this.UpdateContractorsList();
            this.LoadView();
        }
        public void DeleteContractor(int contractorId)
        {
            string sql = $"DELETE FROM Contractors " +
                         $"WHERE ContractorId = {contractorId}";

            using (var conn = DbConnection.GetInstance().Connect())
            {
                conn.Execute(sql);
            }
            this.UpdateContractorsList();
            this.LoadView();
        }
 
    }
}
