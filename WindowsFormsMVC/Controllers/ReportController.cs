﻿using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsMVC.Interfaces;
using WindowsFormsMVC.Models;

namespace WindowsFormsMVC.Controllers
{
    public class ReportController
    {
        IReportView _view;
        
        public ReportController(IReportView view)
        {
            _view = view;
            _view.SetController(this);
        }
        /* 
         * Metoda wypełnia textBoxy wartościami modelu,
         * gettery zdefiniowane w klasie widoku 
         */
        public void UpdateViewDetailValues(ReportModel report)
        {
            _view.SaleSum          = report.SaleSum;
            _view.OtherIncomesSum  = report.OtherIncomesSum;
            _view.TotalIncomesSum  = report.TotalIncomesSum;
            _view.PurchaseSum      = report.PurchaseSum;
            _view.PurchaseCostSum  = report.PurchaseCostSum;
            _view.SalarySum        = report.SalarySum;
            _view.OtherOutcomesSum = report.OtherOutcomesSum;
            _view.TotalOutcomesSum = report.TotalOutcomesSum;
        }
        /* 
         * Metoda przypisuje sumy poszczególnych kolumn, 
         * jako wartości poszczególnych właściwości modelu
         */ 
        public void UpdateModelWithDbValues(ReportModel report)
        {
            IList temp;
            string currentDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
            string dateTo = string.IsNullOrEmpty(report.DateTo) ? currentDate : report.DateTo;
            string sql =    "SELECT " +
                            "SUM(Sale) as SaleSum, " +
                            "SUM(OtherIncomes) as OtherIncomesSum, " +
                            "SUM(TotalIncomes) as TotalIncomesSum, " +
                            "SUM(Purchase) as PurchaseSum, " +
                            "SUM(PurchaseCost) as PurchaseCostSum, " +
                            "SUM(Salary) as SalarySum, " +
                            "SUM(OtherOutComes) as OtherOutcomesSum, " +
                            "SUM(TotalOutcomes) as TotalOutcomesSum " +
                            "FROM Documents " +
                            $"WHERE DocumentCreateDate > '{report.DateFrom}' " +
                            $"AND   DocumentCreateDate < '{dateTo}'";
            using (var conn = DbConnection.GetInstance().Connect())
            {
                temp = conn.Query<ReportModel>(sql).ToList();
            }
            
            foreach(ReportModel tempReport in temp)
            {
                report.SaleSum          = tempReport.SaleSum;
                report.OtherIncomesSum  = tempReport.OtherIncomesSum;
                report.TotalIncomesSum  = tempReport.TotalIncomesSum;
                report.PurchaseSum      = tempReport.PurchaseSum;
                report.PurchaseCostSum  = tempReport.PurchaseCostSum;
                report.SalarySum        = tempReport.SalarySum;
                report.OtherOutcomesSum = tempReport.OtherOutcomesSum;
                report.TotalOutcomesSum = tempReport.TotalOutcomesSum;             
            }
        }
        public void SetReportDateValue(ReportModel report)
        {
            report.DateFrom = _view.DateFrom;
            report.DateTo   = _view.DateTo;
        }
        public void LoadView()
        {
            ReportModel reportModel = new ReportModel();
            UpdateModelWithDbValues(reportModel);
            UpdateViewDetailValues(reportModel);
        }
    }
}
