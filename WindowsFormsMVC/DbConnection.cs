﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsMVC
{
    public class DbConnection
    {
        private string _connectionString = @"__CONNECTION_STRING__";
        private SqlConnection _connection;

        private static DbConnection instance;
        private DbConnection() { }
        public static DbConnection GetInstance()
        {
            if (instance == null)
                instance = new DbConnection();

            return instance;
        }
        public SqlConnection Connect()
        {
            _connection = new SqlConnection(_connectionString);
            return _connection;
        }
    }
}
