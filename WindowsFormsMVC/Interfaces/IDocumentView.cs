﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsMVC.Controllers;
using WindowsFormsMVC.Models;

namespace WindowsFormsMVC.Interfaces
{
    public interface IDocumentView
    {
        void SetController(DocumentController controller);
        void ClearGrid();
        void AddDocumentToGrid(DocumentModel document);
        void SetSelectedDocumentInGrid(DocumentModel document);
        string DocumentNumber { get; set; }
        string Descript { get; set; }
        double Sale { get; set; }
        double OtherIncomes { get; set; }
        double TotalIncomes { get; set; }
        double Purchase { get; set; }
        double PurchaseCost { get; set; }
        double Salary { get; set; }
        double OtherOutcomes { get; set; }
        double TotalOutcomes { get; set; }
        string ShortName { get; }
    }
}
