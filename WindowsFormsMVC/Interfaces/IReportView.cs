﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsMVC.Controllers;

namespace WindowsFormsMVC.Interfaces
{
    public interface IReportView
    {
        void SetController(ReportController controller);
        double SaleSum { get; set; }
        double OtherIncomesSum { get; set; }
        double TotalIncomesSum { get; set; }
        double PurchaseSum { get; set; }
        double PurchaseCostSum { get; set; }
        double SalarySum { get; set; }
        double OtherOutcomesSum { get; set; }
        double TotalOutcomesSum { get; set; }
        string DateFrom { get; }
        string DateTo { get; }
    }
}
