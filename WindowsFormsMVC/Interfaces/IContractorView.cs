﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsMVC.Controllers;
using WindowsFormsMVC.Models;

namespace WindowsFormsMVC.Interfaces
{
    public interface IContractorView
    {
        void SetController(ContractorController controller);
        void AddContractorToGrid(ContractorModel contractor);
        void ClearGrid();
        void SetSelectedContractorInGrid(ContractorModel contractor);
        string ShortName { get; set; }
        string FullName { get; set; }
        string Country { get; set; }
        string City { get; set; }
        string NIP { get; set; }
    }
}
