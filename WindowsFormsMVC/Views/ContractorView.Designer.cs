﻿namespace WindowsFormsMVC.Views
{
    partial class ContractorView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContractorView));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonDeleteContracotr = new System.Windows.Forms.Button();
            this.buttonUpdateContractor = new System.Windows.Forms.Button();
            this.buttonAddContractor = new System.Windows.Forms.Button();
            this.panelFullName = new System.Windows.Forms.Panel();
            this.labelFullName = new System.Windows.Forms.Label();
            this.richTextBoxFullName = new System.Windows.Forms.RichTextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panelCity = new System.Windows.Forms.Panel();
            this.labelCity = new System.Windows.Forms.Label();
            this.textBoxCity = new System.Windows.Forms.TextBox();
            this.panelCountry = new System.Windows.Forms.Panel();
            this.labelCountry = new System.Windows.Forms.Label();
            this.textBoxCountry = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panelNIP = new System.Windows.Forms.Panel();
            this.labelNIP = new System.Windows.Forms.Label();
            this.textBoxNIP = new System.Windows.Forms.TextBox();
            this.panelShortName = new System.Windows.Forms.Panel();
            this.labelShortName = new System.Windows.Forms.Label();
            this.textBoxShortName = new System.Windows.Forms.TextBox();
            this.listViewContractors = new System.Windows.Forms.ListView();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelFullName.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelCity.SuspendLayout();
            this.panelCountry.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panelNIP.SuspendLayout();
            this.panelShortName.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panelFullName);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Location = new System.Drawing.Point(50, 15);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 140);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonDeleteContracotr);
            this.panel2.Controls.Add(this.buttonUpdateContractor);
            this.panel2.Controls.Add(this.buttonAddContractor);
            this.panel2.Location = new System.Drawing.Point(635, 10);
            this.panel2.Margin = new System.Windows.Forms.Padding(5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(145, 120);
            this.panel2.TabIndex = 7;
            // 
            // buttonDeleteContracotr
            // 
            this.buttonDeleteContracotr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(45)))), ((int)(((byte)(25)))));
            this.buttonDeleteContracotr.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonDeleteContracotr.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonDeleteContracotr.Location = new System.Drawing.Point(14, 82);
            this.buttonDeleteContracotr.Name = "buttonDeleteContracotr";
            this.buttonDeleteContracotr.Size = new System.Drawing.Size(120, 30);
            this.buttonDeleteContracotr.TabIndex = 8;
            this.buttonDeleteContracotr.Text = "USUŃ";
            this.buttonDeleteContracotr.UseVisualStyleBackColor = false;
            this.buttonDeleteContracotr.Click += new System.EventHandler(this.buttonDeleteContracotr_Click);
            // 
            // buttonUpdateContractor
            // 
            this.buttonUpdateContractor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(160)))), ((int)(((byte)(20)))));
            this.buttonUpdateContractor.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonUpdateContractor.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonUpdateContractor.Location = new System.Drawing.Point(14, 46);
            this.buttonUpdateContractor.Name = "buttonUpdateContractor";
            this.buttonUpdateContractor.Size = new System.Drawing.Size(120, 30);
            this.buttonUpdateContractor.TabIndex = 7;
            this.buttonUpdateContractor.Text = "AKTUALIZUJ";
            this.buttonUpdateContractor.UseVisualStyleBackColor = false;
            this.buttonUpdateContractor.Click += new System.EventHandler(this.buttonUpdateContractor_Click);
            // 
            // buttonAddContractor
            // 
            this.buttonAddContractor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(160)))), ((int)(((byte)(70)))));
            this.buttonAddContractor.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddContractor.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonAddContractor.Location = new System.Drawing.Point(14, 10);
            this.buttonAddContractor.Name = "buttonAddContractor";
            this.buttonAddContractor.Size = new System.Drawing.Size(120, 30);
            this.buttonAddContractor.TabIndex = 6;
            this.buttonAddContractor.Text = "DODAJ";
            this.buttonAddContractor.UseVisualStyleBackColor = false;
            this.buttonAddContractor.Click += new System.EventHandler(this.buttonAddContractor_Click);
            // 
            // panelFullName
            // 
            this.panelFullName.Controls.Add(this.labelFullName);
            this.panelFullName.Controls.Add(this.richTextBoxFullName);
            this.panelFullName.Location = new System.Drawing.Point(420, 10);
            this.panelFullName.Margin = new System.Windows.Forms.Padding(10);
            this.panelFullName.Name = "panelFullName";
            this.panelFullName.Size = new System.Drawing.Size(200, 120);
            this.panelFullName.TabIndex = 5;
            // 
            // labelFullName
            // 
            this.labelFullName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelFullName.AutoSize = true;
            this.labelFullName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFullName.Location = new System.Drawing.Point(50, 7);
            this.labelFullName.Name = "labelFullName";
            this.labelFullName.Size = new System.Drawing.Size(99, 18);
            this.labelFullName.TabIndex = 2;
            this.labelFullName.Text = "PEŁNA NAZWA";
            this.labelFullName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // richTextBoxFullName
            // 
            this.richTextBoxFullName.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.richTextBoxFullName.Location = new System.Drawing.Point(10, 27);
            this.richTextBoxFullName.Margin = new System.Windows.Forms.Padding(5);
            this.richTextBoxFullName.Name = "richTextBoxFullName";
            this.richTextBoxFullName.Size = new System.Drawing.Size(180, 78);
            this.richTextBoxFullName.TabIndex = 0;
            this.richTextBoxFullName.Text = "";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panelCity);
            this.panel3.Controls.Add(this.panelCountry);
            this.panel3.Location = new System.Drawing.Point(215, 10);
            this.panel3.Margin = new System.Windows.Forms.Padding(10);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 120);
            this.panel3.TabIndex = 4;
            // 
            // panelCity
            // 
            this.panelCity.Controls.Add(this.labelCity);
            this.panelCity.Controls.Add(this.textBoxCity);
            this.panelCity.Location = new System.Drawing.Point(10, 60);
            this.panelCity.Margin = new System.Windows.Forms.Padding(5);
            this.panelCity.Name = "panelCity";
            this.panelCity.Size = new System.Drawing.Size(180, 50);
            this.panelCity.TabIndex = 2;
            // 
            // labelCity
            // 
            this.labelCity.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelCity.AutoSize = true;
            this.labelCity.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCity.Location = new System.Drawing.Point(62, 2);
            this.labelCity.Name = "labelCity";
            this.labelCity.Size = new System.Drawing.Size(58, 18);
            this.labelCity.TabIndex = 1;
            this.labelCity.Text = "MIASTO";
            this.labelCity.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxCity
            // 
            this.textBoxCity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxCity.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxCity.Location = new System.Drawing.Point(5, 22);
            this.textBoxCity.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxCity.Name = "textBoxCity";
            this.textBoxCity.Size = new System.Drawing.Size(170, 23);
            this.textBoxCity.TabIndex = 0;
            // 
            // panelCountry
            // 
            this.panelCountry.Controls.Add(this.labelCountry);
            this.panelCountry.Controls.Add(this.textBoxCountry);
            this.panelCountry.Location = new System.Drawing.Point(10, 5);
            this.panelCountry.Margin = new System.Windows.Forms.Padding(5);
            this.panelCountry.Name = "panelCountry";
            this.panelCountry.Size = new System.Drawing.Size(180, 50);
            this.panelCountry.TabIndex = 0;
            // 
            // labelCountry
            // 
            this.labelCountry.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelCountry.AutoSize = true;
            this.labelCountry.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCountry.Location = new System.Drawing.Point(72, 2);
            this.labelCountry.Name = "labelCountry";
            this.labelCountry.Size = new System.Drawing.Size(38, 18);
            this.labelCountry.TabIndex = 1;
            this.labelCountry.Text = "KRAJ";
            this.labelCountry.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxCountry
            // 
            this.textBoxCountry.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxCountry.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxCountry.Location = new System.Drawing.Point(5, 22);
            this.textBoxCountry.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxCountry.Name = "textBoxCountry";
            this.textBoxCountry.Size = new System.Drawing.Size(170, 23);
            this.textBoxCountry.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panelNIP);
            this.panel4.Controls.Add(this.panelShortName);
            this.panel4.Location = new System.Drawing.Point(10, 10);
            this.panel4.Margin = new System.Windows.Forms.Padding(10);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 120);
            this.panel4.TabIndex = 3;
            // 
            // panelNIP
            // 
            this.panelNIP.Controls.Add(this.labelNIP);
            this.panelNIP.Controls.Add(this.textBoxNIP);
            this.panelNIP.Location = new System.Drawing.Point(10, 60);
            this.panelNIP.Margin = new System.Windows.Forms.Padding(5);
            this.panelNIP.Name = "panelNIP";
            this.panelNIP.Size = new System.Drawing.Size(180, 50);
            this.panelNIP.TabIndex = 2;
            // 
            // labelNIP
            // 
            this.labelNIP.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelNIP.AutoSize = true;
            this.labelNIP.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelNIP.Location = new System.Drawing.Point(64, 2);
            this.labelNIP.Name = "labelNIP";
            this.labelNIP.Size = new System.Drawing.Size(30, 18);
            this.labelNIP.TabIndex = 1;
            this.labelNIP.Text = "NIP";
            this.labelNIP.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxNIP
            // 
            this.textBoxNIP.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxNIP.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxNIP.Location = new System.Drawing.Point(5, 22);
            this.textBoxNIP.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxNIP.Name = "textBoxNIP";
            this.textBoxNIP.Size = new System.Drawing.Size(144, 23);
            this.textBoxNIP.TabIndex = 0;
            // 
            // panelShortName
            // 
            this.panelShortName.Controls.Add(this.labelShortName);
            this.panelShortName.Controls.Add(this.textBoxShortName);
            this.panelShortName.Location = new System.Drawing.Point(10, 5);
            this.panelShortName.Margin = new System.Windows.Forms.Padding(5);
            this.panelShortName.Name = "panelShortName";
            this.panelShortName.Size = new System.Drawing.Size(180, 50);
            this.panelShortName.TabIndex = 0;
            // 
            // labelShortName
            // 
            this.labelShortName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelShortName.AutoSize = true;
            this.labelShortName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelShortName.Location = new System.Drawing.Point(25, 2);
            this.labelShortName.Name = "labelShortName";
            this.labelShortName.Size = new System.Drawing.Size(109, 18);
            this.labelShortName.TabIndex = 1;
            this.labelShortName.Text = "KRÓTKA NAZWA";
            this.labelShortName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxShortName
            // 
            this.textBoxShortName.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxShortName.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxShortName.Location = new System.Drawing.Point(5, 22);
            this.textBoxShortName.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxShortName.Name = "textBoxShortName";
            this.textBoxShortName.Size = new System.Drawing.Size(144, 23);
            this.textBoxShortName.TabIndex = 0;
            // 
            // listViewContractors
            // 
            this.listViewContractors.BackColor = System.Drawing.Color.LightGray;
            this.listViewContractors.Cursor = System.Windows.Forms.Cursors.Hand;
            this.listViewContractors.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listViewContractors.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listViewContractors.FullRowSelect = true;
            this.listViewContractors.GridLines = true;
            this.listViewContractors.HideSelection = false;
            this.listViewContractors.Location = new System.Drawing.Point(0, 236);
            this.listViewContractors.Name = "listViewContractors";
            this.listViewContractors.Size = new System.Drawing.Size(884, 214);
            this.listViewContractors.TabIndex = 1;
            this.listViewContractors.UseCompatibleStateImageBehavior = false;
            this.listViewContractors.View = System.Windows.Forms.View.Details;
            this.listViewContractors.SelectedIndexChanged += new System.EventHandler(this.listViewContractors_SelectedIndexChanged);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // ContractorView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(884, 450);
            this.Controls.Add(this.listViewContractors);
            this.Controls.Add(this.panel1);
            this.Name = "ContractorView";
            this.Text = "Kontrahenci";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ContractorView_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panelFullName.ResumeLayout(false);
            this.panelFullName.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panelCity.ResumeLayout(false);
            this.panelCity.PerformLayout();
            this.panelCountry.ResumeLayout(false);
            this.panelCountry.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panelNIP.ResumeLayout(false);
            this.panelNIP.PerformLayout();
            this.panelShortName.ResumeLayout(false);
            this.panelShortName.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panelShortName;
        private System.Windows.Forms.Label labelShortName;
        private System.Windows.Forms.TextBox textBoxShortName;
        private System.Windows.Forms.Panel panelNIP;
        private System.Windows.Forms.Label labelNIP;
        private System.Windows.Forms.TextBox textBoxNIP;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panelCity;
        private System.Windows.Forms.Label labelCity;
        private System.Windows.Forms.TextBox textBoxCity;
        private System.Windows.Forms.Panel panelCountry;
        private System.Windows.Forms.Label labelCountry;
        private System.Windows.Forms.TextBox textBoxCountry;
        private System.Windows.Forms.Panel panelFullName;
        private System.Windows.Forms.Label labelFullName;
        private System.Windows.Forms.RichTextBox richTextBoxFullName;
        private System.Windows.Forms.ListView listViewContractors;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonDeleteContracotr;
        private System.Windows.Forms.Button buttonUpdateContractor;
        private System.Windows.Forms.Button buttonAddContractor;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}