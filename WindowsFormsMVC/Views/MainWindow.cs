﻿using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsMVC.Controllers;
using WindowsFormsMVC.Interfaces;
using WindowsFormsMVC.Models;

namespace WindowsFormsMVC.Views
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        /* Metody 'open%' są przypisane do zdarzenia 'Click' 
         * kontrolek: panel, label i pictureBox z nazwą danej formatki,
         * po to żeby cały obszar panelu był klikalny
         */
        private void openDocuments(object sender, EventArgs e)
        {
            DocumentView view = new DocumentView();
            IList documents;  
            string sql = "SELECT * FROM Documents d " +
                         "INNER JOIN Contractors c " +
                         "ON c.ContractorId = d.ContractorId";

            using (var conn = DbConnection.GetInstance().Connect())
            {
                documents = conn.Query<DocumentModel>(sql).ToList();
            }
            DocumentController controller = new DocumentController(view, documents);
            controller.LoadView();
            this.Hide();
            view.Show();
        }
        private void openContractors(object sender, EventArgs e)
        {
            ContractorView view = new ContractorView();
            IList contractors;
            string sql = "SELECT * FROM Contractors";

            using (var conn = DbConnection.GetInstance().Connect())
            {
                contractors = conn.Query<ContractorModel>(sql).ToList();
            }
            ContractorController controller = new ContractorController(view, contractors);
            controller.LoadView();
            this.Hide();
            view.Show();
        }
        private void openReports(object sender, EventArgs e)
        {
            ReportView view = new ReportView();
            ReportController controller = new ReportController(view);
            controller.LoadView();
            this.Hide();
            view.Show();
        }
        
        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(0);
        }
   
    }
}
