﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsMVC.Controllers;
using WindowsFormsMVC.Interfaces;
using WindowsFormsMVC.Models;

namespace WindowsFormsMVC.Views
{
    public partial class DocumentView : Form, IDocumentView
    {
        public DocumentView()
        {
          
            InitializeComponent();
        }

        DocumentController _controller;

        #region IDocumentView_implementation
        public string DocumentNumber 
        {
            get { return this.textBoxDocumentNumber.Text; }
            set { this.textBoxDocumentNumber.Text = value; }
        }
        public string Descript 
        {
            get { return this.richTextBoxDescription.Text; }
            set { this.richTextBoxDescription.Text = value; }
        }
        public string ShortName
        {
            get { return this.comboBoxContractors.SelectedItem.ToString(); }
        }
        public double Sale 
        {
            get { return Double.Parse(this.textBoxSale.Text); }
            set { this.textBoxSale.Text = value.ToString(); }
        }
        public double OtherIncomes 
        {
            get { return Double.Parse(this.textBoxOtherIncomes.Text); }
            set { this.textBoxOtherIncomes.Text = value.ToString(); }
        }
        public double TotalIncomes 
        {
            get { return Double.Parse(this.textBoxTotalIncomes.Text); }
            set { this.textBoxTotalIncomes.Text = value.ToString(); }
        }
        public double Purchase 
        {
            get { return Double.Parse(this.textBoxPurchase.Text); }
            set { this.textBoxPurchase.Text = value.ToString(); }
        }
        public double PurchaseCost 
        {
            get { return Double.Parse(this.textBoxPurchaseCost.Text); }
            set { this.textBoxPurchaseCost.Text = value.ToString(); }
        }
        public double Salary 
        {
            get { return Double.Parse(this.textBoxSalary.Text); }
            set { this.textBoxSalary.Text = value.ToString(); }
        }
        public double OtherOutcomes 
        {
            get { return Double.Parse(this.textBoxOtherOutcomes.Text); }
            set { this.textBoxOtherOutcomes.Text = value.ToString(); }
        }
        public double TotalOutcomes 
        {
            get { return Double.Parse(this.textBoxTotalOutcomes.Text); }
            set { this.textBoxTotalOutcomes.Text = value.ToString(); }
        }

        public void AddDocumentToGrid(DocumentModel document)
        {
            ListViewItem viewItem;
            viewItem = this.listViewDocuments.Items.Add(document.DocumentId.ToString());
            viewItem.SubItems.Add(document.DocumentNumber);
            viewItem.SubItems.Add(document.DocumentCreateDate);
            viewItem.SubItems.Add(document.ShortName);
            viewItem.SubItems.Add(document.Descript);
            viewItem.SubItems.Add(document.Sale.ToString());
            viewItem.SubItems.Add(document.OtherIncomes.ToString());
            viewItem.SubItems.Add(document.TotalIncomes.ToString());
            viewItem.SubItems.Add(document.Purchase.ToString());
            viewItem.SubItems.Add(document.PurchaseCost.ToString());
            viewItem.SubItems.Add(document.Salary.ToString());
            viewItem.SubItems.Add(document.OtherOutcomes.ToString());
            viewItem.SubItems.Add(document.TotalOutcomes.ToString());
        }

        public void ClearGrid()
        {
            this.listViewDocuments.Columns.Clear();

            this.listViewDocuments.Columns.Add("ID", 40, HorizontalAlignment.Left);
            this.listViewDocuments.Columns.Add("Numer Dokumentu", 100, HorizontalAlignment.Left);
            this.listViewDocuments.Columns.Add("Data Utworzenia", 100, HorizontalAlignment.Left);
            this.listViewDocuments.Columns.Add("Kontrahent", 100, HorizontalAlignment.Left);
            this.listViewDocuments.Columns.Add("Opis", 100, HorizontalAlignment.Left);
            this.listViewDocuments.Columns.Add("Sprzedaż", 100, HorizontalAlignment.Left);
            this.listViewDocuments.Columns.Add("Pozostałe przychody", 100, HorizontalAlignment.Left);
            this.listViewDocuments.Columns.Add("Razem przychód", 100, HorizontalAlignment.Left);
            this.listViewDocuments.Columns.Add("Zakup", 100, HorizontalAlignment.Left);
            this.listViewDocuments.Columns.Add("Koszty zakupu", 100, HorizontalAlignment.Left);
            this.listViewDocuments.Columns.Add("Wynagrodzenia", 100, HorizontalAlignment.Left);
            this.listViewDocuments.Columns.Add("Pozostałe wydatki", 100, HorizontalAlignment.Left);
            this.listViewDocuments.Columns.Add("Razem wydatki", 100, HorizontalAlignment.Left);

            this.listViewDocuments.Items.Clear();
        }
        public void SetController(DocumentController controller)
        {
            this._controller = controller;
        }
        public void SetSelectedDocumentInGrid(DocumentModel document)
        {
            foreach (ListViewItem row in this.listViewDocuments.Items)
            {
                if (row.Text == document.DocumentId.ToString())
                {
                    row.Selected = true;
                }
            }
        }
        #endregion

        private void listViewDocuments_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listViewDocuments.SelectedItems.Count > 0)
            {
                _controller.SelectedDocumentChanged(this.listViewDocuments.SelectedItems[0].Text);
            }
        }
        private void buttonAddDocument_Click(object sender, EventArgs e)
        {
              try
              {
                  _controller.AddNewDocument();
              }
              catch (Exception)
              {
                  MessageBox.Show("Pola zawierają nieprawidłowe wartości");
              }
        }
        private void buttonUpdateDocument_Click(object sender, EventArgs e)
        {
            if (this.listViewDocuments.SelectedItems.Count > 0)
            {
                int documentId = Int32.Parse(this.listViewDocuments.SelectedItems[0].Text);
                 
                try
                  {
                    _controller.UpdateDocument(documentId);
                  }
                  catch(Exception)
                  {
                      MessageBox.Show("Pola zawierają nieprawidłowe wartości");
                  }
            }
        }
        private void buttonDeleteDocument_Click(object sender, EventArgs e)
        {
            if (this.listViewDocuments.SelectedItems.Count > 0)
            {
                int documentId = Int32.Parse(this.listViewDocuments.SelectedItems[0].Text);
                string message = "Czy na pewno chcesz usunąć dokument?";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;

                result = MessageBox.Show(message, "Komunikat", buttons);
                if (result == DialogResult.Yes)
                {
                        _controller.DeleteDocument(documentId);
                }
                else return;
            }
        }
        //metody 'setTotal%' przypisują poszczególnym textBoxom sumę wartości innych textBoxów
        private void setTotalIncomesValue(object sender, EventArgs e)
        {
            double saleValue, otherIncomesValue;

            if (Double.TryParse(this.textBoxSale.Text, out saleValue))
            {
                saleValue = Double.Parse(this.textBoxSale.Text);
            }
            if (Double.TryParse(this.textBoxOtherIncomes.Text, out otherIncomesValue))
            {
                otherIncomesValue = Double.Parse(this.textBoxOtherIncomes.Text);
            }

            this.textBoxTotalIncomes.Text = (saleValue + otherIncomesValue).ToString();
        }

        private void setTotalOutcomesValue(object sender, EventArgs e)
        {
            double salaryValue, otherOutcomesValue;

            if (Double.TryParse(this.textBoxSalary.Text, out salaryValue))
            {
                salaryValue = Double.Parse(this.textBoxSalary.Text);
            }
            if (Double.TryParse(this.textBoxOtherOutcomes.Text, out otherOutcomesValue))
            {
                otherOutcomesValue = Double.Parse(this.textBoxOtherOutcomes.Text);
            }

            this.textBoxTotalOutcomes.Text = (salaryValue + otherOutcomesValue).ToString();
        }
        //
        private void DocumentView_FormClosing(object sender, FormClosingEventArgs e)
        {
            new MainWindow().Show();
        }

        private void DocumentView_Load(object sender, EventArgs e)
        {
            _controller.CheckContractors();
            List<string> names = _controller.GetContractorsNames();

            if (names.Count > 0)
                this.comboBoxContractors.DataSource = _controller.GetContractorsNames();
            else
            {
                MessageBox.Show("Brak kontrahentów. Aby dodać nowy dokument, dodaj najpierw kontrahenta.");
                this.Close();
            }
            
        }
    }
}
