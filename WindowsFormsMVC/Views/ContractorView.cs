﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsMVC.Controllers;
using WindowsFormsMVC.Interfaces;
using WindowsFormsMVC.Models;

namespace WindowsFormsMVC.Views
{
    public partial class ContractorView : Form, IContractorView
    {
        public ContractorView()
        {
            InitializeComponent();
        }
        ContractorController _controller;
        #region iContracotrView_implementation
        public string ShortName 
        {
            get { return this.textBoxShortName.Text; }
            set { this.textBoxShortName.Text = value; }
        }
        public string FullName 
        {
            get { return this.richTextBoxFullName.Text; }
            set { this.richTextBoxFullName.Text = value; }
        }
        public string Country 
        {
            get { return this.textBoxCountry.Text; }
            set { this.textBoxCountry.Text = value; }  
        }
        public string City 
        {
            get { return this.textBoxCity.Text; }
            set { this.textBoxCity.Text = value; }
        }
        public string NIP 
        {
            get { return this.textBoxNIP.Text; }
            set { this.textBoxNIP.Text = value; }  
        }

        public void AddContractorToGrid(ContractorModel contractor)
        {
            ListViewItem viewItem;
            viewItem = this.listViewContractors.Items.Add(contractor.ContractorId.ToString());
            viewItem.SubItems.Add(contractor.ShortName);
            viewItem.SubItems.Add(contractor.NIP);
            viewItem.SubItems.Add(contractor.FullName);
            viewItem.SubItems.Add(contractor.Country);
            viewItem.SubItems.Add(contractor.City);
            viewItem.SubItems.Add(contractor.ContractorCreateDate);
        }

        public void ClearGrid()
        {
            this.listViewContractors.Columns.Clear();

            this.listViewContractors.Columns.Add("ID", 40, HorizontalAlignment.Left);
            this.listViewContractors.Columns.Add("Krótka Nazwa", 100, HorizontalAlignment.Left);
            this.listViewContractors.Columns.Add("NIP", 100, HorizontalAlignment.Left);
            this.listViewContractors.Columns.Add("Pełna nazwa", 100, HorizontalAlignment.Left);
            this.listViewContractors.Columns.Add("Kraj", 100, HorizontalAlignment.Left);
            this.listViewContractors.Columns.Add("Miasto", 100, HorizontalAlignment.Left);
            this.listViewContractors.Columns.Add("Data utworzenia", 100, HorizontalAlignment.Left);

            this.listViewContractors.Items.Clear();
        }

        public void SetSelectedContractorInGrid(ContractorModel contractor)
        {
            foreach (ListViewItem row in this.listViewContractors.Items)
            {
                if (row.Text == contractor.ContractorId.ToString())
                {
                    row.Selected = true;
                }
            }
        }

        public void SetController(ContractorController controller)
        {
            _controller = controller;
        }
        #endregion

        private void ContractorView_FormClosing(object sender, FormClosingEventArgs e)
        {
            new MainWindow().Show();
        }

        private void listViewContractors_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listViewContractors.SelectedItems.Count > 0)
            {
                _controller.SelectedContractorChanged(this.listViewContractors.SelectedItems[0].Text);
            }
        }

        private void buttonAddContractor_Click(object sender, EventArgs e)
        {
            Regex nipRegex = new Regex(@"^((\d{3}[- ]\d{3}[- ]\d{2}[- ]\d{2}))$");
            if(!nipRegex.IsMatch(this.textBoxNIP.Text))
            {
                this.errorProvider1.SetError(this.textBoxNIP, "NIP powinien mieć format: XXX-XXX-XX-XX");
                return;
            }
            _controller.AddNewContractor();
            MessageBox.Show("Kontrahent został dodany");
        }

        private void buttonDeleteContracotr_Click(object sender, EventArgs e)
        {
            if (this.listViewContractors.SelectedItems.Count > 0)
            {
                int contractorId = Int32.Parse(this.listViewContractors.SelectedItems[0].Text);
                string message = "Czy na pewno chcesz usunąć tego kontrahenta?";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;

                result = MessageBox.Show(message,"Komunikat", buttons);
                if (result == DialogResult.Yes)
                {
                    try
                    {
                        _controller.DeleteContractor(contractorId);
                    }
                    catch(SqlException sqlEx)
                    {
                        MessageBox.Show("Nie możesz usunąć kontrahenta ponieważ istenieją dokumenty z nim powiązane." +
                                        "Aby go usunąć, usuń najpierw dokumenty");
                    }
                }
                else return;          
            }
        }

        private void buttonUpdateContractor_Click(object sender, EventArgs e)
        {
            if (this.listViewContractors.SelectedItems.Count > 0)
            {
                int contractorId = Int32.Parse(this.listViewContractors.SelectedItems[0].Text);
                _controller.UpdateContractor(contractorId);
            }
        }
    }
}
