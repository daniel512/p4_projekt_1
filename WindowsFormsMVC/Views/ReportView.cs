﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsMVC.Controllers;
using WindowsFormsMVC.Interfaces;
using WindowsFormsMVC.Models;

namespace WindowsFormsMVC.Views
{
    public partial class ReportView : Form, IReportView
    {
        public ReportView()
        {
            InitializeComponent();
        }

        public double SaleSum 
        {
            get { return Double.Parse(this.textBoxSaleSum.Text); }
            set { this.textBoxSaleSum.Text = value.ToString(); }
        }
        public double OtherIncomesSum 
        {
            get { return Double.Parse(this.textBoxOtherIncomesSum.Text); }
            set { this.textBoxOtherIncomesSum.Text = value.ToString(); }
        }
        public double TotalIncomesSum 
        {
            get { return Double.Parse(this.textBoxTotalIncomesSum.Text); }
            set { this.textBoxTotalIncomesSum.Text = value.ToString(); }
        }
        public double PurchaseSum 
        {
            get { return Double.Parse(this.textBoxPurchaseSum.Text); }
            set { this.textBoxPurchaseSum.Text = value.ToString(); }
        }
        public double PurchaseCostSum 
        {
            get { return Double.Parse(this.textBoxPurchaseCostSum.Text); }
            set { this.textBoxPurchaseCostSum.Text = value.ToString(); }
        }
        public double SalarySum 
        {
            get { return Double.Parse(this.textBoxSalarySum.Text); }
            set { this.textBoxSalarySum.Text = value.ToString(); }
        }
        public double OtherOutcomesSum 
        {
            get { return Double.Parse(this.textBoxOtherOutcomesSum.Text); }
            set { this.textBoxOtherOutcomesSum.Text = value.ToString(); }
        }
        public double TotalOutcomesSum 
        {
            get { return Double.Parse(this.textBoxTotalOutcomesSum.Text); }
            set { this.textBoxTotalOutcomesSum.Text = value.ToString(); }
        }
        public string DateFrom 
        {
            get { return this.dateTimePickerFrom.Text; }
        }
        public string DateTo 
        {
            get { return this.dateTimePickerTo.Text; }
        }

        ReportController _controller;
        public void SetController(ReportController controller)
        {
            _controller = controller;
        }

        private void ReportsView_FormClosing(object sender, FormClosingEventArgs e)
        {
            new MainWindow().Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ReportModel report = new ReportModel();
            _controller.SetReportDateValue(report);
            _controller.UpdateModelWithDbValues(report);
            using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "PDF file|*.pdf", ValidateNames = true })
            {
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.A4.Rotate());
                    try
                    {
                        PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(sfd.FileName, FileMode.Create));
                        doc.Open();
                        string dateFrom    = report.DateFrom;
                        string dateTo      = report.DateTo;
                        Paragraph title = new Paragraph($"Podsumowanie KPiR za okres od {dateFrom} do {dateTo}")
                        {
                            SpacingAfter = 30,
                            Alignment = iTextSharp.text.Element.ALIGN_CENTER
                        };
                   
                        doc.Add(title);

                        var table = new PdfPTable(8);

                        /* Komórka nagłówkowa dla komórek nagłówkowych przychodów */
                        PdfPCell incomes = new PdfPCell(new Phrase("Przychody"))
                        {
                            Colspan = 3
                        };

                        PdfPCell costs = new PdfPCell(new Phrase("Zakup towarów"))
                        {
                            Rowspan = 2
                        }; 
                        PdfPCell otherCosts = new PdfPCell(new Phrase("Koszty uboczne zakupu")) 
                        {
                            Rowspan = 2
                        };

                        /* Komórka nagłówkowa dla komórek nagłówkowych wydatków */
                        PdfPCell outcomes = new PdfPCell(new Phrase("Wydatki"))
                        {
                            Colspan = 3
                        };
                        
                        
                        PdfPCell sale = new PdfPCell(new Phrase("Sprzedaz towarów i uslug"));

                        /*
                         * Dla ośmiu kolumn razem 10 komórek nagłówkowych 
                         * (2 dodatkowe komórki nagłówkowe dla przychodów i wydatków)
                         * Komórki dodajemy kolejno od lewej do prawej 
                         */
                        table.AddCell(incomes);
                        table.AddCell(costs);
                        table.AddCell(otherCosts);
                        table.AddCell(outcomes);
                        table.AddCell(sale);
                        table.AddCell("Pozostale przychody");
                        table.AddCell("Razem Przychód");                       
                        table.AddCell("Wynagrodzenia");
                        table.AddCell("Pozostale wydatki");
                        table.AddCell("Razem wydatki");

                        /* Dodawanie odpowienich wartości do kolejnych komórek */
                        table.AddCell(report.SaleSum.ToString());
                        table.AddCell(report.OtherIncomesSum.ToString());
                        table.AddCell(report.TotalIncomesSum.ToString());
                        table.AddCell(report.PurchaseSum.ToString());
                        table.AddCell(report.PurchaseCostSum.ToString());
                        table.AddCell(report.SalarySum.ToString());
                        table.AddCell(report.OtherOutcomesSum.ToString());
                        table.AddCell(report.TotalOutcomesSum.ToString());

                        doc.Add(table);
                        MessageBox.Show("Plik został zapisany");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        doc.Close();
                    }
                }
            }
        }

    }
}
