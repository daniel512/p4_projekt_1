﻿namespace WindowsFormsMVC.Views
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelReports = new System.Windows.Forms.Panel();
            this.labelReports = new System.Windows.Forms.Label();
            this.pictureBoxReports = new System.Windows.Forms.PictureBox();
            this.panelContractors = new System.Windows.Forms.Panel();
            this.labelContractors = new System.Windows.Forms.Label();
            this.pictureBoxContractors = new System.Windows.Forms.PictureBox();
            this.panelDocuments = new System.Windows.Forms.Panel();
            this.labelDocuments = new System.Windows.Forms.Label();
            this.pictureBoxDocuments = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panelReports.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxReports)).BeginInit();
            this.panelContractors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxContractors)).BeginInit();
            this.panelDocuments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDocuments)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.Controls.Add(this.panelReports);
            this.panel1.Controls.Add(this.panelContractors);
            this.panel1.Controls.Add(this.panelDocuments);
            this.panel1.Location = new System.Drawing.Point(50, 43);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(330, 90);
            this.panel1.TabIndex = 3;
            // 
            // panelReports
            // 
            this.panelReports.Controls.Add(this.labelReports);
            this.panelReports.Controls.Add(this.pictureBoxReports);
            this.panelReports.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelReports.Location = new System.Drawing.Point(228, 10);
            this.panelReports.Name = "panelReports";
            this.panelReports.Size = new System.Drawing.Size(90, 70);
            this.panelReports.TabIndex = 3;
            this.panelReports.Click += new System.EventHandler(this.openReports);
            // 
            // labelReports
            // 
            this.labelReports.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelReports.AutoSize = true;
            this.labelReports.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelReports.Location = new System.Drawing.Point(15, 53);
            this.labelReports.Name = "labelReports";
            this.labelReports.Size = new System.Drawing.Size(58, 15);
            this.labelReports.TabIndex = 1;
            this.labelReports.Text = "RAPORTY";
            this.labelReports.Click += new System.EventHandler(this.openReports);
            // 
            // pictureBoxReports
            // 
            this.pictureBoxReports.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBoxReports.Image = global::WindowsFormsMVC.Properties.Resources.reports;
            this.pictureBoxReports.InitialImage = null;
            this.pictureBoxReports.Location = new System.Drawing.Point(20, 0);
            this.pictureBoxReports.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBoxReports.Name = "pictureBoxReports";
            this.pictureBoxReports.Size = new System.Drawing.Size(50, 50);
            this.pictureBoxReports.TabIndex = 1;
            this.pictureBoxReports.TabStop = false;
            this.pictureBoxReports.Click += new System.EventHandler(this.openReports);
            // 
            // panelContractors
            // 
            this.panelContractors.Controls.Add(this.labelContractors);
            this.panelContractors.Controls.Add(this.pictureBoxContractors);
            this.panelContractors.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelContractors.Location = new System.Drawing.Point(123, 10);
            this.panelContractors.Name = "panelContractors";
            this.panelContractors.Size = new System.Drawing.Size(90, 70);
            this.panelContractors.TabIndex = 2;
            this.panelContractors.Click += new System.EventHandler(this.openContractors);
            // 
            // labelContractors
            // 
            this.labelContractors.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelContractors.AutoSize = true;
            this.labelContractors.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelContractors.Location = new System.Drawing.Point(2, 53);
            this.labelContractors.Name = "labelContractors";
            this.labelContractors.Size = new System.Drawing.Size(85, 15);
            this.labelContractors.TabIndex = 1;
            this.labelContractors.Text = "KONTRAHENCI";
            this.labelContractors.Click += new System.EventHandler(this.openContractors);
            // 
            // pictureBoxContractors
            // 
            this.pictureBoxContractors.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBoxContractors.Image = global::WindowsFormsMVC.Properties.Resources.contractors;
            this.pictureBoxContractors.InitialImage = null;
            this.pictureBoxContractors.Location = new System.Drawing.Point(20, 0);
            this.pictureBoxContractors.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBoxContractors.Name = "pictureBoxContractors";
            this.pictureBoxContractors.Size = new System.Drawing.Size(50, 50);
            this.pictureBoxContractors.TabIndex = 1;
            this.pictureBoxContractors.TabStop = false;
            this.pictureBoxContractors.Click += new System.EventHandler(this.openContractors);
            // 
            // panelDocuments
            // 
            this.panelDocuments.Controls.Add(this.labelDocuments);
            this.panelDocuments.Controls.Add(this.pictureBoxDocuments);
            this.panelDocuments.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelDocuments.Location = new System.Drawing.Point(17, 10);
            this.panelDocuments.Name = "panelDocuments";
            this.panelDocuments.Size = new System.Drawing.Size(90, 70);
            this.panelDocuments.TabIndex = 0;
            this.panelDocuments.Click += new System.EventHandler(this.openDocuments);
            // 
            // labelDocuments
            // 
            this.labelDocuments.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelDocuments.AutoSize = true;
            this.labelDocuments.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDocuments.Location = new System.Drawing.Point(6, 53);
            this.labelDocuments.Name = "labelDocuments";
            this.labelDocuments.Size = new System.Drawing.Size(78, 15);
            this.labelDocuments.TabIndex = 1;
            this.labelDocuments.Text = "DOKUMENTY";
            this.labelDocuments.Click += new System.EventHandler(this.openDocuments);
            // 
            // pictureBoxDocuments
            // 
            this.pictureBoxDocuments.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBoxDocuments.Image = global::WindowsFormsMVC.Properties.Resources.document;
            this.pictureBoxDocuments.InitialImage = null;
            this.pictureBoxDocuments.Location = new System.Drawing.Point(20, 0);
            this.pictureBoxDocuments.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBoxDocuments.Name = "pictureBoxDocuments";
            this.pictureBoxDocuments.Size = new System.Drawing.Size(50, 50);
            this.pictureBoxDocuments.TabIndex = 1;
            this.pictureBoxDocuments.TabStop = false;
            this.pictureBoxDocuments.Click += new System.EventHandler(this.openDocuments);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WindowsFormsMVC.Properties.Resources.city_bg;
            this.ClientSize = new System.Drawing.Size(414, 201);
            this.Controls.Add(this.panel1);
            this.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.Name = "MainWindow";
            this.Text = "MikroAccounts 1.0";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindow_FormClosed);
            this.panel1.ResumeLayout(false);
            this.panelReports.ResumeLayout(false);
            this.panelReports.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxReports)).EndInit();
            this.panelContractors.ResumeLayout(false);
            this.panelContractors.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxContractors)).EndInit();
            this.panelDocuments.ResumeLayout(false);
            this.panelDocuments.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDocuments)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelDocuments;
        private System.Windows.Forms.PictureBox pictureBoxDocuments;
        private System.Windows.Forms.Panel panelContractors;
        private System.Windows.Forms.Label labelContractors;
        private System.Windows.Forms.PictureBox pictureBoxContractors;
        private System.Windows.Forms.Label labelDocuments;
        private System.Windows.Forms.Panel panelReports;
        private System.Windows.Forms.Label labelReports;
        private System.Windows.Forms.PictureBox pictureBoxReports;
    }
}