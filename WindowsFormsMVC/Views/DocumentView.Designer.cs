﻿namespace WindowsFormsMVC.Views
{
    partial class DocumentView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewDocuments = new System.Windows.Forms.ListView();
            this.panelMain = new System.Windows.Forms.Panel();
            this.panelButtons = new System.Windows.Forms.Panel();
            this.buttonDeleteDocument = new System.Windows.Forms.Button();
            this.buttonUpdateDocument = new System.Windows.Forms.Button();
            this.buttonAddDocument = new System.Windows.Forms.Button();
            this.panelDescription = new System.Windows.Forms.Panel();
            this.richTextBoxDescription = new System.Windows.Forms.RichTextBox();
            this.labelDescription = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panelOutcomes = new System.Windows.Forms.Panel();
            this.panelTotalOutcomes = new System.Windows.Forms.Panel();
            this.labelTotalOutcomes = new System.Windows.Forms.Label();
            this.textBoxTotalOutcomes = new System.Windows.Forms.TextBox();
            this.panelSalary = new System.Windows.Forms.Panel();
            this.labelSalary = new System.Windows.Forms.Label();
            this.textBoxSalary = new System.Windows.Forms.TextBox();
            this.panelOtherOutcomes = new System.Windows.Forms.Panel();
            this.labelOtherOutcomes = new System.Windows.Forms.Label();
            this.textBoxOtherOutcomes = new System.Windows.Forms.TextBox();
            this.panelPurchases = new System.Windows.Forms.Panel();
            this.panelPurchase = new System.Windows.Forms.Panel();
            this.labelPurchase = new System.Windows.Forms.Label();
            this.textBoxPurchase = new System.Windows.Forms.TextBox();
            this.panelPurchaseCost = new System.Windows.Forms.Panel();
            this.labelPurchaseCost = new System.Windows.Forms.Label();
            this.textBoxPurchaseCost = new System.Windows.Forms.TextBox();
            this.panelIncomes = new System.Windows.Forms.Panel();
            this.panelTotalIncomes = new System.Windows.Forms.Panel();
            this.labelTotalIncomes = new System.Windows.Forms.Label();
            this.textBoxTotalIncomes = new System.Windows.Forms.TextBox();
            this.panelSale = new System.Windows.Forms.Panel();
            this.labelSale = new System.Windows.Forms.Label();
            this.textBoxSale = new System.Windows.Forms.TextBox();
            this.panelOtherIncomes = new System.Windows.Forms.Panel();
            this.labelOtherIncomes = new System.Windows.Forms.Label();
            this.textBoxOtherIncomes = new System.Windows.Forms.TextBox();
            this.panelInputs = new System.Windows.Forms.Panel();
            this.panelNIP = new System.Windows.Forms.Panel();
            this.comboBoxContractors = new System.Windows.Forms.ComboBox();
            this.labelContractor = new System.Windows.Forms.Label();
            this.panelDocumentNumber = new System.Windows.Forms.Panel();
            this.labelDocumentNumber = new System.Windows.Forms.Label();
            this.textBoxDocumentNumber = new System.Windows.Forms.TextBox();
            this.panelMain.SuspendLayout();
            this.panelButtons.SuspendLayout();
            this.panelDescription.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelOutcomes.SuspendLayout();
            this.panelTotalOutcomes.SuspendLayout();
            this.panelSalary.SuspendLayout();
            this.panelOtherOutcomes.SuspendLayout();
            this.panelPurchases.SuspendLayout();
            this.panelPurchase.SuspendLayout();
            this.panelPurchaseCost.SuspendLayout();
            this.panelIncomes.SuspendLayout();
            this.panelTotalIncomes.SuspendLayout();
            this.panelSale.SuspendLayout();
            this.panelOtherIncomes.SuspendLayout();
            this.panelInputs.SuspendLayout();
            this.panelNIP.SuspendLayout();
            this.panelDocumentNumber.SuspendLayout();
            this.SuspendLayout();
            // 
            // listViewDocuments
            // 
            this.listViewDocuments.BackColor = System.Drawing.Color.LightGray;
            this.listViewDocuments.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listViewDocuments.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listViewDocuments.ForeColor = System.Drawing.SystemColors.WindowText;
            this.listViewDocuments.FullRowSelect = true;
            this.listViewDocuments.GridLines = true;
            this.listViewDocuments.HideSelection = false;
            this.listViewDocuments.Location = new System.Drawing.Point(0, 236);
            this.listViewDocuments.Name = "listViewDocuments";
            this.listViewDocuments.Size = new System.Drawing.Size(944, 214);
            this.listViewDocuments.TabIndex = 0;
            this.listViewDocuments.UseCompatibleStateImageBehavior = false;
            this.listViewDocuments.View = System.Windows.Forms.View.Details;
            this.listViewDocuments.SelectedIndexChanged += new System.EventHandler(this.listViewDocuments_SelectedIndexChanged);
            // 
            // panelMain
            // 
            this.panelMain.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panelMain.BackColor = System.Drawing.Color.LightGray;
            this.panelMain.Controls.Add(this.panelButtons);
            this.panelMain.Controls.Add(this.panelDescription);
            this.panelMain.Controls.Add(this.panel2);
            this.panelMain.Controls.Add(this.panelInputs);
            this.panelMain.Location = new System.Drawing.Point(45, 15);
            this.panelMain.Margin = new System.Windows.Forms.Padding(5);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(860, 213);
            this.panelMain.TabIndex = 1;
            // 
            // panelButtons
            // 
            this.panelButtons.Controls.Add(this.buttonDeleteDocument);
            this.panelButtons.Controls.Add(this.buttonUpdateDocument);
            this.panelButtons.Controls.Add(this.buttonAddDocument);
            this.panelButtons.Location = new System.Drawing.Point(354, 10);
            this.panelButtons.Name = "panelButtons";
            this.panelButtons.Size = new System.Drawing.Size(200, 115);
            this.panelButtons.TabIndex = 8;
            // 
            // buttonDeleteDocument
            // 
            this.buttonDeleteDocument.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(45)))), ((int)(((byte)(25)))));
            this.buttonDeleteDocument.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonDeleteDocument.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonDeleteDocument.Location = new System.Drawing.Point(40, 78);
            this.buttonDeleteDocument.Name = "buttonDeleteDocument";
            this.buttonDeleteDocument.Size = new System.Drawing.Size(120, 30);
            this.buttonDeleteDocument.TabIndex = 9;
            this.buttonDeleteDocument.Text = "USUŃ";
            this.buttonDeleteDocument.UseVisualStyleBackColor = false;
            this.buttonDeleteDocument.Click += new System.EventHandler(this.buttonDeleteDocument_Click);
            // 
            // buttonUpdateDocument
            // 
            this.buttonUpdateDocument.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(160)))), ((int)(((byte)(20)))));
            this.buttonUpdateDocument.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonUpdateDocument.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonUpdateDocument.Location = new System.Drawing.Point(40, 42);
            this.buttonUpdateDocument.Name = "buttonUpdateDocument";
            this.buttonUpdateDocument.Size = new System.Drawing.Size(120, 30);
            this.buttonUpdateDocument.TabIndex = 8;
            this.buttonUpdateDocument.Text = "AKTUALIZUJ";
            this.buttonUpdateDocument.UseVisualStyleBackColor = false;
            this.buttonUpdateDocument.Click += new System.EventHandler(this.buttonUpdateDocument_Click);
            // 
            // buttonAddDocument
            // 
            this.buttonAddDocument.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(160)))), ((int)(((byte)(70)))));
            this.buttonAddDocument.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddDocument.ForeColor = System.Drawing.SystemColors.Control;
            this.buttonAddDocument.Location = new System.Drawing.Point(40, 5);
            this.buttonAddDocument.Name = "buttonAddDocument";
            this.buttonAddDocument.Size = new System.Drawing.Size(120, 30);
            this.buttonAddDocument.TabIndex = 7;
            this.buttonAddDocument.Text = "DODAJ";
            this.buttonAddDocument.UseVisualStyleBackColor = false;
            this.buttonAddDocument.Click += new System.EventHandler(this.buttonAddDocument_Click);
            // 
            // panelDescription
            // 
            this.panelDescription.Controls.Add(this.richTextBoxDescription);
            this.panelDescription.Controls.Add(this.labelDescription);
            this.panelDescription.Location = new System.Drawing.Point(195, 10);
            this.panelDescription.Margin = new System.Windows.Forms.Padding(10);
            this.panelDescription.Name = "panelDescription";
            this.panelDescription.Size = new System.Drawing.Size(155, 115);
            this.panelDescription.TabIndex = 5;
            // 
            // richTextBoxDescription
            // 
            this.richTextBoxDescription.Location = new System.Drawing.Point(8, 27);
            this.richTextBoxDescription.Name = "richTextBoxDescription";
            this.richTextBoxDescription.Size = new System.Drawing.Size(140, 76);
            this.richTextBoxDescription.TabIndex = 7;
            this.richTextBoxDescription.Text = "";
            // 
            // labelDescription
            // 
            this.labelDescription.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelDescription.AutoSize = true;
            this.labelDescription.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDescription.Location = new System.Drawing.Point(64, 10);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(30, 14);
            this.labelDescription.TabIndex = 6;
            this.labelDescription.Text = "OPIS";
            this.labelDescription.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panelOutcomes);
            this.panel2.Controls.Add(this.panelPurchases);
            this.panel2.Controls.Add(this.panelIncomes);
            this.panel2.Location = new System.Drawing.Point(10, 128);
            this.panel2.Margin = new System.Windows.Forms.Padding(10);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(840, 70);
            this.panel2.TabIndex = 5;
            // 
            // panelOutcomes
            // 
            this.panelOutcomes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.panelOutcomes.Controls.Add(this.panelTotalOutcomes);
            this.panelOutcomes.Controls.Add(this.panelSalary);
            this.panelOutcomes.Controls.Add(this.panelOtherOutcomes);
            this.panelOutcomes.Location = new System.Drawing.Point(526, 3);
            this.panelOutcomes.Name = "panelOutcomes";
            this.panelOutcomes.Size = new System.Drawing.Size(311, 60);
            this.panelOutcomes.TabIndex = 5;
            // 
            // panelTotalOutcomes
            // 
            this.panelTotalOutcomes.Controls.Add(this.labelTotalOutcomes);
            this.panelTotalOutcomes.Controls.Add(this.textBoxTotalOutcomes);
            this.panelTotalOutcomes.Location = new System.Drawing.Point(219, 5);
            this.panelTotalOutcomes.Margin = new System.Windows.Forms.Padding(2);
            this.panelTotalOutcomes.Name = "panelTotalOutcomes";
            this.panelTotalOutcomes.Size = new System.Drawing.Size(88, 50);
            this.panelTotalOutcomes.TabIndex = 4;
            // 
            // labelTotalOutcomes
            // 
            this.labelTotalOutcomes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelTotalOutcomes.AutoSize = true;
            this.labelTotalOutcomes.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTotalOutcomes.Location = new System.Drawing.Point(2, 6);
            this.labelTotalOutcomes.Name = "labelTotalOutcomes";
            this.labelTotalOutcomes.Size = new System.Drawing.Size(86, 18);
            this.labelTotalOutcomes.TabIndex = 1;
            this.labelTotalOutcomes.Text = "RAZEM WYDATKI";
            this.labelTotalOutcomes.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelTotalOutcomes.UseCompatibleTextRendering = true;
            // 
            // textBoxTotalOutcomes
            // 
            this.textBoxTotalOutcomes.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxTotalOutcomes.Enabled = false;
            this.textBoxTotalOutcomes.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxTotalOutcomes.Location = new System.Drawing.Point(7, 24);
            this.textBoxTotalOutcomes.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxTotalOutcomes.Name = "textBoxTotalOutcomes";
            this.textBoxTotalOutcomes.Size = new System.Drawing.Size(70, 22);
            this.textBoxTotalOutcomes.TabIndex = 0;
            this.textBoxTotalOutcomes.Text = "0";
            // 
            // panelSalary
            // 
            this.panelSalary.Controls.Add(this.labelSalary);
            this.panelSalary.Controls.Add(this.textBoxSalary);
            this.panelSalary.Location = new System.Drawing.Point(2, 5);
            this.panelSalary.Margin = new System.Windows.Forms.Padding(2);
            this.panelSalary.Name = "panelSalary";
            this.panelSalary.Size = new System.Drawing.Size(95, 50);
            this.panelSalary.TabIndex = 3;
            // 
            // labelSalary
            // 
            this.labelSalary.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelSalary.AutoSize = true;
            this.labelSalary.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSalary.Location = new System.Drawing.Point(0, 6);
            this.labelSalary.Name = "labelSalary";
            this.labelSalary.Size = new System.Drawing.Size(91, 13);
            this.labelSalary.TabIndex = 1;
            this.labelSalary.Text = "WYNAGRODZENIA";
            this.labelSalary.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxSalary
            // 
            this.textBoxSalary.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxSalary.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxSalary.Location = new System.Drawing.Point(8, 24);
            this.textBoxSalary.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxSalary.Name = "textBoxSalary";
            this.textBoxSalary.Size = new System.Drawing.Size(70, 22);
            this.textBoxSalary.TabIndex = 0;
            this.textBoxSalary.Text = "0";
            this.textBoxSalary.TextChanged += new System.EventHandler(this.setTotalOutcomesValue);
            // 
            // panelOtherOutcomes
            // 
            this.panelOtherOutcomes.Controls.Add(this.labelOtherOutcomes);
            this.panelOtherOutcomes.Controls.Add(this.textBoxOtherOutcomes);
            this.panelOtherOutcomes.Location = new System.Drawing.Point(101, 5);
            this.panelOtherOutcomes.Margin = new System.Windows.Forms.Padding(2);
            this.panelOtherOutcomes.Name = "panelOtherOutcomes";
            this.panelOtherOutcomes.Size = new System.Drawing.Size(114, 50);
            this.panelOtherOutcomes.TabIndex = 3;
            // 
            // labelOtherOutcomes
            // 
            this.labelOtherOutcomes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelOtherOutcomes.AutoSize = true;
            this.labelOtherOutcomes.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOtherOutcomes.Location = new System.Drawing.Point(6, 6);
            this.labelOtherOutcomes.Name = "labelOtherOutcomes";
            this.labelOtherOutcomes.Size = new System.Drawing.Size(107, 18);
            this.labelOtherOutcomes.TabIndex = 1;
            this.labelOtherOutcomes.Text = "POZOSTAŁE WYDATKI";
            this.labelOtherOutcomes.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelOtherOutcomes.UseCompatibleTextRendering = true;
            // 
            // textBoxOtherOutcomes
            // 
            this.textBoxOtherOutcomes.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxOtherOutcomes.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxOtherOutcomes.Location = new System.Drawing.Point(20, 24);
            this.textBoxOtherOutcomes.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxOtherOutcomes.Name = "textBoxOtherOutcomes";
            this.textBoxOtherOutcomes.Size = new System.Drawing.Size(70, 22);
            this.textBoxOtherOutcomes.TabIndex = 0;
            this.textBoxOtherOutcomes.Text = "0";
            this.textBoxOtherOutcomes.TextChanged += new System.EventHandler(this.setTotalOutcomesValue);
            // 
            // panelPurchases
            // 
            this.panelPurchases.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.panelPurchases.Controls.Add(this.panelPurchase);
            this.panelPurchases.Controls.Add(this.panelPurchaseCost);
            this.panelPurchases.Location = new System.Drawing.Point(341, 3);
            this.panelPurchases.Name = "panelPurchases";
            this.panelPurchases.Size = new System.Drawing.Size(179, 60);
            this.panelPurchases.TabIndex = 4;
            // 
            // panelPurchase
            // 
            this.panelPurchase.Controls.Add(this.labelPurchase);
            this.panelPurchase.Controls.Add(this.textBoxPurchase);
            this.panelPurchase.Location = new System.Drawing.Point(3, 5);
            this.panelPurchase.Margin = new System.Windows.Forms.Padding(2);
            this.panelPurchase.Name = "panelPurchase";
            this.panelPurchase.Size = new System.Drawing.Size(83, 50);
            this.panelPurchase.TabIndex = 2;
            // 
            // labelPurchase
            // 
            this.labelPurchase.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelPurchase.AutoSize = true;
            this.labelPurchase.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPurchase.Location = new System.Drawing.Point(21, 6);
            this.labelPurchase.Name = "labelPurchase";
            this.labelPurchase.Size = new System.Drawing.Size(38, 13);
            this.labelPurchase.TabIndex = 1;
            this.labelPurchase.Text = "ZAKUP";
            this.labelPurchase.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxPurchase
            // 
            this.textBoxPurchase.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxPurchase.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxPurchase.Location = new System.Drawing.Point(8, 24);
            this.textBoxPurchase.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxPurchase.Name = "textBoxPurchase";
            this.textBoxPurchase.Size = new System.Drawing.Size(70, 22);
            this.textBoxPurchase.TabIndex = 0;
            this.textBoxPurchase.Text = "0";
            // 
            // panelPurchaseCost
            // 
            this.panelPurchaseCost.Controls.Add(this.labelPurchaseCost);
            this.panelPurchaseCost.Controls.Add(this.textBoxPurchaseCost);
            this.panelPurchaseCost.Location = new System.Drawing.Point(88, 5);
            this.panelPurchaseCost.Margin = new System.Windows.Forms.Padding(2);
            this.panelPurchaseCost.Name = "panelPurchaseCost";
            this.panelPurchaseCost.Size = new System.Drawing.Size(85, 50);
            this.panelPurchaseCost.TabIndex = 4;
            // 
            // labelPurchaseCost
            // 
            this.labelPurchaseCost.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelPurchaseCost.AutoSize = true;
            this.labelPurchaseCost.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPurchaseCost.Location = new System.Drawing.Point(2, 6);
            this.labelPurchaseCost.Name = "labelPurchaseCost";
            this.labelPurchaseCost.Size = new System.Drawing.Size(81, 13);
            this.labelPurchaseCost.TabIndex = 1;
            this.labelPurchaseCost.Text = "KOSZTY ZAKUPU";
            this.labelPurchaseCost.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxPurchaseCost
            // 
            this.textBoxPurchaseCost.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxPurchaseCost.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxPurchaseCost.Location = new System.Drawing.Point(5, 24);
            this.textBoxPurchaseCost.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxPurchaseCost.Name = "textBoxPurchaseCost";
            this.textBoxPurchaseCost.Size = new System.Drawing.Size(70, 22);
            this.textBoxPurchaseCost.TabIndex = 0;
            this.textBoxPurchaseCost.Text = "0";
            // 
            // panelIncomes
            // 
            this.panelIncomes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panelIncomes.Controls.Add(this.panelTotalIncomes);
            this.panelIncomes.Controls.Add(this.panelSale);
            this.panelIncomes.Controls.Add(this.panelOtherIncomes);
            this.panelIncomes.Location = new System.Drawing.Point(20, 3);
            this.panelIncomes.Name = "panelIncomes";
            this.panelIncomes.Size = new System.Drawing.Size(315, 60);
            this.panelIncomes.TabIndex = 3;
            // 
            // panelTotalIncomes
            // 
            this.panelTotalIncomes.Controls.Add(this.labelTotalIncomes);
            this.panelTotalIncomes.Controls.Add(this.textBoxTotalIncomes);
            this.panelTotalIncomes.Location = new System.Drawing.Point(213, 5);
            this.panelTotalIncomes.Margin = new System.Windows.Forms.Padding(2);
            this.panelTotalIncomes.Name = "panelTotalIncomes";
            this.panelTotalIncomes.Size = new System.Drawing.Size(100, 50);
            this.panelTotalIncomes.TabIndex = 4;
            // 
            // labelTotalIncomes
            // 
            this.labelTotalIncomes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelTotalIncomes.AutoSize = true;
            this.labelTotalIncomes.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTotalIncomes.Location = new System.Drawing.Point(2, 6);
            this.labelTotalIncomes.Margin = new System.Windows.Forms.Padding(0);
            this.labelTotalIncomes.Name = "labelTotalIncomes";
            this.labelTotalIncomes.Size = new System.Drawing.Size(98, 18);
            this.labelTotalIncomes.TabIndex = 1;
            this.labelTotalIncomes.Text = "RAZEM PRZYCHODY";
            this.labelTotalIncomes.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelTotalIncomes.UseCompatibleTextRendering = true;
            // 
            // textBoxTotalIncomes
            // 
            this.textBoxTotalIncomes.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxTotalIncomes.Enabled = false;
            this.textBoxTotalIncomes.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxTotalIncomes.Location = new System.Drawing.Point(13, 24);
            this.textBoxTotalIncomes.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxTotalIncomes.Name = "textBoxTotalIncomes";
            this.textBoxTotalIncomes.Size = new System.Drawing.Size(70, 22);
            this.textBoxTotalIncomes.TabIndex = 0;
            this.textBoxTotalIncomes.Text = "0";
            // 
            // panelSale
            // 
            this.panelSale.Controls.Add(this.labelSale);
            this.panelSale.Controls.Add(this.textBoxSale);
            this.panelSale.Location = new System.Drawing.Point(2, 5);
            this.panelSale.Margin = new System.Windows.Forms.Padding(2);
            this.panelSale.Name = "panelSale";
            this.panelSale.Size = new System.Drawing.Size(83, 50);
            this.panelSale.TabIndex = 0;
            // 
            // labelSale
            // 
            this.labelSale.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelSale.AutoSize = true;
            this.labelSale.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSale.Location = new System.Drawing.Point(15, 6);
            this.labelSale.Name = "labelSale";
            this.labelSale.Size = new System.Drawing.Size(53, 13);
            this.labelSale.TabIndex = 1;
            this.labelSale.Text = "SPRZEDAŻ";
            this.labelSale.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxSale
            // 
            this.textBoxSale.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxSale.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxSale.Location = new System.Drawing.Point(8, 24);
            this.textBoxSale.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxSale.Name = "textBoxSale";
            this.textBoxSale.Size = new System.Drawing.Size(70, 22);
            this.textBoxSale.TabIndex = 0;
            this.textBoxSale.Text = "0";
            this.textBoxSale.TextChanged += new System.EventHandler(this.setTotalIncomesValue);
            // 
            // panelOtherIncomes
            // 
            this.panelOtherIncomes.Controls.Add(this.labelOtherIncomes);
            this.panelOtherIncomes.Controls.Add(this.textBoxOtherIncomes);
            this.panelOtherIncomes.Location = new System.Drawing.Point(89, 5);
            this.panelOtherIncomes.Margin = new System.Windows.Forms.Padding(2);
            this.panelOtherIncomes.Name = "panelOtherIncomes";
            this.panelOtherIncomes.Size = new System.Drawing.Size(121, 50);
            this.panelOtherIncomes.TabIndex = 2;
            // 
            // labelOtherIncomes
            // 
            this.labelOtherIncomes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelOtherIncomes.AutoSize = true;
            this.labelOtherIncomes.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOtherIncomes.Location = new System.Drawing.Point(5, 6);
            this.labelOtherIncomes.Margin = new System.Windows.Forms.Padding(0);
            this.labelOtherIncomes.Name = "labelOtherIncomes";
            this.labelOtherIncomes.Size = new System.Drawing.Size(116, 13);
            this.labelOtherIncomes.TabIndex = 1;
            this.labelOtherIncomes.Text = "POZOSTAŁE PRZYCHODY";
            this.labelOtherIncomes.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxOtherIncomes
            // 
            this.textBoxOtherIncomes.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxOtherIncomes.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxOtherIncomes.Location = new System.Drawing.Point(31, 24);
            this.textBoxOtherIncomes.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxOtherIncomes.Name = "textBoxOtherIncomes";
            this.textBoxOtherIncomes.Size = new System.Drawing.Size(70, 22);
            this.textBoxOtherIncomes.TabIndex = 0;
            this.textBoxOtherIncomes.Text = "0";
            this.textBoxOtherIncomes.TextChanged += new System.EventHandler(this.setTotalIncomesValue);
            // 
            // panelInputs
            // 
            this.panelInputs.Controls.Add(this.panelNIP);
            this.panelInputs.Controls.Add(this.panelDocumentNumber);
            this.panelInputs.Location = new System.Drawing.Point(10, 10);
            this.panelInputs.Margin = new System.Windows.Forms.Padding(10);
            this.panelInputs.Name = "panelInputs";
            this.panelInputs.Size = new System.Drawing.Size(180, 115);
            this.panelInputs.TabIndex = 4;
            // 
            // panelNIP
            // 
            this.panelNIP.Controls.Add(this.comboBoxContractors);
            this.panelNIP.Controls.Add(this.labelContractor);
            this.panelNIP.Location = new System.Drawing.Point(10, 60);
            this.panelNIP.Margin = new System.Windows.Forms.Padding(5);
            this.panelNIP.Name = "panelNIP";
            this.panelNIP.Size = new System.Drawing.Size(160, 50);
            this.panelNIP.TabIndex = 2;
            // 
            // comboBoxContractors
            // 
            this.comboBoxContractors.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxContractors.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.comboBoxContractors.FormattingEnabled = true;
            this.comboBoxContractors.Location = new System.Drawing.Point(10, 21);
            this.comboBoxContractors.Margin = new System.Windows.Forms.Padding(5);
            this.comboBoxContractors.Name = "comboBoxContractors";
            this.comboBoxContractors.Size = new System.Drawing.Size(140, 22);
            this.comboBoxContractors.TabIndex = 5;
            // 
            // labelContractor
            // 
            this.labelContractor.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelContractor.AutoSize = true;
            this.labelContractor.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelContractor.Location = new System.Drawing.Point(38, 5);
            this.labelContractor.Name = "labelContractor";
            this.labelContractor.Size = new System.Drawing.Size(77, 14);
            this.labelContractor.TabIndex = 1;
            this.labelContractor.Text = "KONTRAHENT";
            this.labelContractor.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panelDocumentNumber
            // 
            this.panelDocumentNumber.Controls.Add(this.labelDocumentNumber);
            this.panelDocumentNumber.Controls.Add(this.textBoxDocumentNumber);
            this.panelDocumentNumber.Location = new System.Drawing.Point(10, 5);
            this.panelDocumentNumber.Margin = new System.Windows.Forms.Padding(5);
            this.panelDocumentNumber.Name = "panelDocumentNumber";
            this.panelDocumentNumber.Size = new System.Drawing.Size(160, 50);
            this.panelDocumentNumber.TabIndex = 0;
            // 
            // labelDocumentNumber
            // 
            this.labelDocumentNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelDocumentNumber.AutoSize = true;
            this.labelDocumentNumber.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDocumentNumber.Location = new System.Drawing.Point(20, 5);
            this.labelDocumentNumber.Name = "labelDocumentNumber";
            this.labelDocumentNumber.Size = new System.Drawing.Size(118, 14);
            this.labelDocumentNumber.TabIndex = 1;
            this.labelDocumentNumber.Text = "NUMER DOKUMENTU";
            this.labelDocumentNumber.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxDocumentNumber
            // 
            this.textBoxDocumentNumber.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxDocumentNumber.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxDocumentNumber.Location = new System.Drawing.Point(10, 22);
            this.textBoxDocumentNumber.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxDocumentNumber.Name = "textBoxDocumentNumber";
            this.textBoxDocumentNumber.Size = new System.Drawing.Size(140, 22);
            this.textBoxDocumentNumber.TabIndex = 0;
            // 
            // DocumentView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WindowsFormsMVC.Properties.Resources.clouds_bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(944, 450);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.listViewDocuments);
            this.Name = "DocumentView";
            this.Text = "Dokumenty";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DocumentView_FormClosing);
            this.Load += new System.EventHandler(this.DocumentView_Load);
            this.panelMain.ResumeLayout(false);
            this.panelButtons.ResumeLayout(false);
            this.panelDescription.ResumeLayout(false);
            this.panelDescription.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panelOutcomes.ResumeLayout(false);
            this.panelTotalOutcomes.ResumeLayout(false);
            this.panelTotalOutcomes.PerformLayout();
            this.panelSalary.ResumeLayout(false);
            this.panelSalary.PerformLayout();
            this.panelOtherOutcomes.ResumeLayout(false);
            this.panelOtherOutcomes.PerformLayout();
            this.panelPurchases.ResumeLayout(false);
            this.panelPurchase.ResumeLayout(false);
            this.panelPurchase.PerformLayout();
            this.panelPurchaseCost.ResumeLayout(false);
            this.panelPurchaseCost.PerformLayout();
            this.panelIncomes.ResumeLayout(false);
            this.panelTotalIncomes.ResumeLayout(false);
            this.panelTotalIncomes.PerformLayout();
            this.panelSale.ResumeLayout(false);
            this.panelSale.PerformLayout();
            this.panelOtherIncomes.ResumeLayout(false);
            this.panelOtherIncomes.PerformLayout();
            this.panelInputs.ResumeLayout(false);
            this.panelNIP.ResumeLayout(false);
            this.panelNIP.PerformLayout();
            this.panelDocumentNumber.ResumeLayout(false);
            this.panelDocumentNumber.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listViewDocuments;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Panel panelInputs;
        private System.Windows.Forms.Panel panelNIP;
        private System.Windows.Forms.ComboBox comboBoxContractors;
        private System.Windows.Forms.Label labelContractor;
        private System.Windows.Forms.Panel panelDocumentNumber;
        private System.Windows.Forms.Label labelDocumentNumber;
        private System.Windows.Forms.TextBox textBoxDocumentNumber;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panelSalary;
        private System.Windows.Forms.Label labelSalary;
        private System.Windows.Forms.TextBox textBoxSalary;
        private System.Windows.Forms.Panel panelOtherOutcomes;
        private System.Windows.Forms.Label labelOtherOutcomes;
        private System.Windows.Forms.TextBox textBoxOtherOutcomes;
        private System.Windows.Forms.Panel panelPurchase;
        private System.Windows.Forms.Label labelPurchase;
        private System.Windows.Forms.TextBox textBoxPurchase;
        private System.Windows.Forms.Panel panelOtherIncomes;
        private System.Windows.Forms.Label labelOtherIncomes;
        private System.Windows.Forms.TextBox textBoxOtherIncomes;
        private System.Windows.Forms.Panel panelSale;
        private System.Windows.Forms.Label labelSale;
        private System.Windows.Forms.TextBox textBoxSale;
        private System.Windows.Forms.Panel panelPurchaseCost;
        private System.Windows.Forms.Label labelPurchaseCost;
        private System.Windows.Forms.TextBox textBoxPurchaseCost;
        private System.Windows.Forms.Panel panelDescription;
        private System.Windows.Forms.RichTextBox richTextBoxDescription;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.Panel panelIncomes;
        private System.Windows.Forms.Panel panelTotalIncomes;
        private System.Windows.Forms.Label labelTotalIncomes;
        private System.Windows.Forms.TextBox textBoxTotalIncomes;
        private System.Windows.Forms.Panel panelPurchases;
        private System.Windows.Forms.Panel panelOutcomes;
        private System.Windows.Forms.Panel panelTotalOutcomes;
        private System.Windows.Forms.Label labelTotalOutcomes;
        private System.Windows.Forms.TextBox textBoxTotalOutcomes;
        private System.Windows.Forms.Panel panelButtons;
        private System.Windows.Forms.Button buttonAddDocument;
        private System.Windows.Forms.Button buttonUpdateDocument;
        private System.Windows.Forms.Button buttonDeleteDocument;
    }
}