﻿namespace WindowsFormsMVC.Views
{
    partial class ReportView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.panelIncomes = new System.Windows.Forms.Panel();
            this.panelTotalIncomes = new System.Windows.Forms.Panel();
            this.labelTotalIncomes = new System.Windows.Forms.Label();
            this.textBoxTotalIncomesSum = new System.Windows.Forms.TextBox();
            this.panelSale = new System.Windows.Forms.Panel();
            this.labelSale = new System.Windows.Forms.Label();
            this.textBoxSaleSum = new System.Windows.Forms.TextBox();
            this.panelOtherIncomes = new System.Windows.Forms.Panel();
            this.labelOtherIncomes = new System.Windows.Forms.Label();
            this.textBoxOtherIncomesSum = new System.Windows.Forms.TextBox();
            this.panelPurchases = new System.Windows.Forms.Panel();
            this.panelPurchase = new System.Windows.Forms.Panel();
            this.labelPurchase = new System.Windows.Forms.Label();
            this.textBoxPurchaseSum = new System.Windows.Forms.TextBox();
            this.panelPurchaseCost = new System.Windows.Forms.Panel();
            this.labelPurchaseCost = new System.Windows.Forms.Label();
            this.textBoxPurchaseCostSum = new System.Windows.Forms.TextBox();
            this.panelOutcomes = new System.Windows.Forms.Panel();
            this.panelTotalOutcomes = new System.Windows.Forms.Panel();
            this.labelTotalOutcomes = new System.Windows.Forms.Label();
            this.textBoxTotalOutcomesSum = new System.Windows.Forms.TextBox();
            this.panelSalary = new System.Windows.Forms.Panel();
            this.labelSalary = new System.Windows.Forms.Label();
            this.textBoxSalarySum = new System.Windows.Forms.TextBox();
            this.panelOtherOutcomes = new System.Windows.Forms.Panel();
            this.labelOtherOutcomes = new System.Windows.Forms.Label();
            this.textBoxOtherOutcomesSum = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panelDateTo = new System.Windows.Forms.Panel();
            this.labelDateTo = new System.Windows.Forms.Label();
            this.panelDateFrom = new System.Windows.Forms.Panel();
            this.labelDateFrom = new System.Windows.Forms.Label();
            this.panelIncomes.SuspendLayout();
            this.panelTotalIncomes.SuspendLayout();
            this.panelSale.SuspendLayout();
            this.panelOtherIncomes.SuspendLayout();
            this.panelPurchases.SuspendLayout();
            this.panelPurchase.SuspendLayout();
            this.panelPurchaseCost.SuspendLayout();
            this.panelOutcomes.SuspendLayout();
            this.panelTotalOutcomes.SuspendLayout();
            this.panelSalary.SuspendLayout();
            this.panelOtherOutcomes.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelDateTo.SuspendLayout();
            this.panelDateFrom.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(9, 117);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 40);
            this.button1.TabIndex = 0;
            this.button1.Text = "ZAPISZ DO PDF";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panelIncomes
            // 
            this.panelIncomes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panelIncomes.Controls.Add(this.panelTotalIncomes);
            this.panelIncomes.Controls.Add(this.panelSale);
            this.panelIncomes.Controls.Add(this.panelOtherIncomes);
            this.panelIncomes.Location = new System.Drawing.Point(3, 3);
            this.panelIncomes.Name = "panelIncomes";
            this.panelIncomes.Size = new System.Drawing.Size(315, 60);
            this.panelIncomes.TabIndex = 4;
            // 
            // panelTotalIncomes
            // 
            this.panelTotalIncomes.Controls.Add(this.labelTotalIncomes);
            this.panelTotalIncomes.Controls.Add(this.textBoxTotalIncomesSum);
            this.panelTotalIncomes.Location = new System.Drawing.Point(213, 5);
            this.panelTotalIncomes.Margin = new System.Windows.Forms.Padding(2);
            this.panelTotalIncomes.Name = "panelTotalIncomes";
            this.panelTotalIncomes.Size = new System.Drawing.Size(100, 50);
            this.panelTotalIncomes.TabIndex = 4;
            // 
            // labelTotalIncomes
            // 
            this.labelTotalIncomes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelTotalIncomes.AutoSize = true;
            this.labelTotalIncomes.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTotalIncomes.Location = new System.Drawing.Point(2, 6);
            this.labelTotalIncomes.Margin = new System.Windows.Forms.Padding(0);
            this.labelTotalIncomes.Name = "labelTotalIncomes";
            this.labelTotalIncomes.Size = new System.Drawing.Size(98, 18);
            this.labelTotalIncomes.TabIndex = 1;
            this.labelTotalIncomes.Text = "RAZEM PRZYCHODY";
            this.labelTotalIncomes.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelTotalIncomes.UseCompatibleTextRendering = true;
            // 
            // textBoxTotalIncomesSum
            // 
            this.textBoxTotalIncomesSum.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxTotalIncomesSum.Enabled = false;
            this.textBoxTotalIncomesSum.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxTotalIncomesSum.Location = new System.Drawing.Point(13, 24);
            this.textBoxTotalIncomesSum.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxTotalIncomesSum.Name = "textBoxTotalIncomesSum";
            this.textBoxTotalIncomesSum.Size = new System.Drawing.Size(70, 22);
            this.textBoxTotalIncomesSum.TabIndex = 0;
            this.textBoxTotalIncomesSum.Text = "0";
            // 
            // panelSale
            // 
            this.panelSale.Controls.Add(this.labelSale);
            this.panelSale.Controls.Add(this.textBoxSaleSum);
            this.panelSale.Location = new System.Drawing.Point(2, 5);
            this.panelSale.Margin = new System.Windows.Forms.Padding(2);
            this.panelSale.Name = "panelSale";
            this.panelSale.Size = new System.Drawing.Size(83, 50);
            this.panelSale.TabIndex = 0;
            // 
            // labelSale
            // 
            this.labelSale.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelSale.AutoSize = true;
            this.labelSale.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSale.Location = new System.Drawing.Point(15, 6);
            this.labelSale.Name = "labelSale";
            this.labelSale.Size = new System.Drawing.Size(53, 13);
            this.labelSale.TabIndex = 1;
            this.labelSale.Text = "SPRZEDAŻ";
            this.labelSale.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxSaleSum
            // 
            this.textBoxSaleSum.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxSaleSum.Enabled = false;
            this.textBoxSaleSum.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxSaleSum.Location = new System.Drawing.Point(8, 24);
            this.textBoxSaleSum.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxSaleSum.Name = "textBoxSaleSum";
            this.textBoxSaleSum.Size = new System.Drawing.Size(70, 22);
            this.textBoxSaleSum.TabIndex = 0;
            this.textBoxSaleSum.Text = "0";
            // 
            // panelOtherIncomes
            // 
            this.panelOtherIncomes.Controls.Add(this.labelOtherIncomes);
            this.panelOtherIncomes.Controls.Add(this.textBoxOtherIncomesSum);
            this.panelOtherIncomes.Location = new System.Drawing.Point(89, 5);
            this.panelOtherIncomes.Margin = new System.Windows.Forms.Padding(2);
            this.panelOtherIncomes.Name = "panelOtherIncomes";
            this.panelOtherIncomes.Size = new System.Drawing.Size(121, 50);
            this.panelOtherIncomes.TabIndex = 2;
            // 
            // labelOtherIncomes
            // 
            this.labelOtherIncomes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelOtherIncomes.AutoSize = true;
            this.labelOtherIncomes.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOtherIncomes.Location = new System.Drawing.Point(5, 6);
            this.labelOtherIncomes.Margin = new System.Windows.Forms.Padding(0);
            this.labelOtherIncomes.Name = "labelOtherIncomes";
            this.labelOtherIncomes.Size = new System.Drawing.Size(116, 13);
            this.labelOtherIncomes.TabIndex = 1;
            this.labelOtherIncomes.Text = "POZOSTAŁE PRZYCHODY";
            this.labelOtherIncomes.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxOtherIncomesSum
            // 
            this.textBoxOtherIncomesSum.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxOtherIncomesSum.Enabled = false;
            this.textBoxOtherIncomesSum.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxOtherIncomesSum.Location = new System.Drawing.Point(32, 24);
            this.textBoxOtherIncomesSum.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxOtherIncomesSum.Name = "textBoxOtherIncomesSum";
            this.textBoxOtherIncomesSum.Size = new System.Drawing.Size(70, 22);
            this.textBoxOtherIncomesSum.TabIndex = 0;
            this.textBoxOtherIncomesSum.Text = "0";
            // 
            // panelPurchases
            // 
            this.panelPurchases.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.panelPurchases.Controls.Add(this.panelPurchase);
            this.panelPurchases.Controls.Add(this.panelPurchaseCost);
            this.panelPurchases.Location = new System.Drawing.Point(3, 69);
            this.panelPurchases.Name = "panelPurchases";
            this.panelPurchases.Size = new System.Drawing.Size(179, 60);
            this.panelPurchases.TabIndex = 5;
            // 
            // panelPurchase
            // 
            this.panelPurchase.Controls.Add(this.labelPurchase);
            this.panelPurchase.Controls.Add(this.textBoxPurchaseSum);
            this.panelPurchase.Location = new System.Drawing.Point(3, 5);
            this.panelPurchase.Margin = new System.Windows.Forms.Padding(2);
            this.panelPurchase.Name = "panelPurchase";
            this.panelPurchase.Size = new System.Drawing.Size(83, 50);
            this.panelPurchase.TabIndex = 2;
            // 
            // labelPurchase
            // 
            this.labelPurchase.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelPurchase.AutoSize = true;
            this.labelPurchase.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPurchase.Location = new System.Drawing.Point(21, 6);
            this.labelPurchase.Name = "labelPurchase";
            this.labelPurchase.Size = new System.Drawing.Size(38, 13);
            this.labelPurchase.TabIndex = 1;
            this.labelPurchase.Text = "ZAKUP";
            this.labelPurchase.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxPurchaseSum
            // 
            this.textBoxPurchaseSum.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxPurchaseSum.Enabled = false;
            this.textBoxPurchaseSum.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxPurchaseSum.Location = new System.Drawing.Point(8, 24);
            this.textBoxPurchaseSum.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxPurchaseSum.Name = "textBoxPurchaseSum";
            this.textBoxPurchaseSum.Size = new System.Drawing.Size(70, 22);
            this.textBoxPurchaseSum.TabIndex = 0;
            this.textBoxPurchaseSum.Text = "0";
            // 
            // panelPurchaseCost
            // 
            this.panelPurchaseCost.Controls.Add(this.labelPurchaseCost);
            this.panelPurchaseCost.Controls.Add(this.textBoxPurchaseCostSum);
            this.panelPurchaseCost.Location = new System.Drawing.Point(88, 5);
            this.panelPurchaseCost.Margin = new System.Windows.Forms.Padding(2);
            this.panelPurchaseCost.Name = "panelPurchaseCost";
            this.panelPurchaseCost.Size = new System.Drawing.Size(85, 50);
            this.panelPurchaseCost.TabIndex = 4;
            // 
            // labelPurchaseCost
            // 
            this.labelPurchaseCost.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelPurchaseCost.AutoSize = true;
            this.labelPurchaseCost.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPurchaseCost.Location = new System.Drawing.Point(2, 6);
            this.labelPurchaseCost.Name = "labelPurchaseCost";
            this.labelPurchaseCost.Size = new System.Drawing.Size(81, 13);
            this.labelPurchaseCost.TabIndex = 1;
            this.labelPurchaseCost.Text = "KOSZTY ZAKUPU";
            this.labelPurchaseCost.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxPurchaseCostSum
            // 
            this.textBoxPurchaseCostSum.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxPurchaseCostSum.Enabled = false;
            this.textBoxPurchaseCostSum.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxPurchaseCostSum.Location = new System.Drawing.Point(5, 24);
            this.textBoxPurchaseCostSum.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxPurchaseCostSum.Name = "textBoxPurchaseCostSum";
            this.textBoxPurchaseCostSum.Size = new System.Drawing.Size(70, 22);
            this.textBoxPurchaseCostSum.TabIndex = 0;
            this.textBoxPurchaseCostSum.Text = "0";
            // 
            // panelOutcomes
            // 
            this.panelOutcomes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.panelOutcomes.Controls.Add(this.panelTotalOutcomes);
            this.panelOutcomes.Controls.Add(this.panelSalary);
            this.panelOutcomes.Controls.Add(this.panelOtherOutcomes);
            this.panelOutcomes.Location = new System.Drawing.Point(3, 135);
            this.panelOutcomes.Name = "panelOutcomes";
            this.panelOutcomes.Size = new System.Drawing.Size(311, 60);
            this.panelOutcomes.TabIndex = 6;
            // 
            // panelTotalOutcomes
            // 
            this.panelTotalOutcomes.Controls.Add(this.labelTotalOutcomes);
            this.panelTotalOutcomes.Controls.Add(this.textBoxTotalOutcomesSum);
            this.panelTotalOutcomes.Location = new System.Drawing.Point(219, 5);
            this.panelTotalOutcomes.Margin = new System.Windows.Forms.Padding(2);
            this.panelTotalOutcomes.Name = "panelTotalOutcomes";
            this.panelTotalOutcomes.Size = new System.Drawing.Size(88, 50);
            this.panelTotalOutcomes.TabIndex = 4;
            // 
            // labelTotalOutcomes
            // 
            this.labelTotalOutcomes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelTotalOutcomes.AutoSize = true;
            this.labelTotalOutcomes.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTotalOutcomes.Location = new System.Drawing.Point(2, 6);
            this.labelTotalOutcomes.Name = "labelTotalOutcomes";
            this.labelTotalOutcomes.Size = new System.Drawing.Size(86, 18);
            this.labelTotalOutcomes.TabIndex = 1;
            this.labelTotalOutcomes.Text = "RAZEM WYDATKI";
            this.labelTotalOutcomes.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelTotalOutcomes.UseCompatibleTextRendering = true;
            // 
            // textBoxTotalOutcomesSum
            // 
            this.textBoxTotalOutcomesSum.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxTotalOutcomesSum.Enabled = false;
            this.textBoxTotalOutcomesSum.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxTotalOutcomesSum.Location = new System.Drawing.Point(7, 24);
            this.textBoxTotalOutcomesSum.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxTotalOutcomesSum.Name = "textBoxTotalOutcomesSum";
            this.textBoxTotalOutcomesSum.Size = new System.Drawing.Size(70, 22);
            this.textBoxTotalOutcomesSum.TabIndex = 0;
            this.textBoxTotalOutcomesSum.Text = "0";
            // 
            // panelSalary
            // 
            this.panelSalary.Controls.Add(this.labelSalary);
            this.panelSalary.Controls.Add(this.textBoxSalarySum);
            this.panelSalary.Location = new System.Drawing.Point(2, 5);
            this.panelSalary.Margin = new System.Windows.Forms.Padding(2);
            this.panelSalary.Name = "panelSalary";
            this.panelSalary.Size = new System.Drawing.Size(95, 50);
            this.panelSalary.TabIndex = 3;
            // 
            // labelSalary
            // 
            this.labelSalary.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelSalary.AutoSize = true;
            this.labelSalary.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSalary.Location = new System.Drawing.Point(0, 6);
            this.labelSalary.Name = "labelSalary";
            this.labelSalary.Size = new System.Drawing.Size(91, 13);
            this.labelSalary.TabIndex = 1;
            this.labelSalary.Text = "WYNAGRODZENIA";
            this.labelSalary.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxSalarySum
            // 
            this.textBoxSalarySum.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxSalarySum.Enabled = false;
            this.textBoxSalarySum.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxSalarySum.Location = new System.Drawing.Point(8, 24);
            this.textBoxSalarySum.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxSalarySum.Name = "textBoxSalarySum";
            this.textBoxSalarySum.Size = new System.Drawing.Size(70, 22);
            this.textBoxSalarySum.TabIndex = 0;
            this.textBoxSalarySum.Text = "0";
            // 
            // panelOtherOutcomes
            // 
            this.panelOtherOutcomes.Controls.Add(this.labelOtherOutcomes);
            this.panelOtherOutcomes.Controls.Add(this.textBoxOtherOutcomesSum);
            this.panelOtherOutcomes.Location = new System.Drawing.Point(101, 5);
            this.panelOtherOutcomes.Margin = new System.Windows.Forms.Padding(2);
            this.panelOtherOutcomes.Name = "panelOtherOutcomes";
            this.panelOtherOutcomes.Size = new System.Drawing.Size(114, 50);
            this.panelOtherOutcomes.TabIndex = 3;
            // 
            // labelOtherOutcomes
            // 
            this.labelOtherOutcomes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelOtherOutcomes.AutoSize = true;
            this.labelOtherOutcomes.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOtherOutcomes.Location = new System.Drawing.Point(6, 6);
            this.labelOtherOutcomes.Name = "labelOtherOutcomes";
            this.labelOtherOutcomes.Size = new System.Drawing.Size(107, 18);
            this.labelOtherOutcomes.TabIndex = 1;
            this.labelOtherOutcomes.Text = "POZOSTAŁE WYDATKI";
            this.labelOtherOutcomes.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelOtherOutcomes.UseCompatibleTextRendering = true;
            // 
            // textBoxOtherOutcomesSum
            // 
            this.textBoxOtherOutcomesSum.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxOtherOutcomesSum.Enabled = false;
            this.textBoxOtherOutcomesSum.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxOtherOutcomesSum.Location = new System.Drawing.Point(20, 24);
            this.textBoxOtherOutcomesSum.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxOtherOutcomesSum.Name = "textBoxOtherOutcomesSum";
            this.textBoxOtherOutcomesSum.Size = new System.Drawing.Size(70, 22);
            this.textBoxOtherOutcomesSum.TabIndex = 0;
            this.textBoxOtherOutcomesSum.Text = "0";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panelIncomes);
            this.panel1.Controls.Add(this.panelOutcomes);
            this.panel1.Controls.Add(this.panelPurchases);
            this.panel1.Location = new System.Drawing.Point(3, 14);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(332, 202);
            this.panel1.TabIndex = 7;
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.CalendarFont = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dateTimePickerFrom.CustomFormat = "yyyy-MM-dd";
            this.dateTimePickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerFrom.Location = new System.Drawing.Point(0, 22);
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.Size = new System.Drawing.Size(120, 20);
            this.dateTimePickerFrom.TabIndex = 8;
            this.dateTimePickerFrom.Value = new System.DateTime(2020, 1, 1, 0, 0, 0, 0);
            // 
            // dateTimePickerTo
            // 
            this.dateTimePickerTo.CalendarFont = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dateTimePickerTo.CustomFormat = "yyyy-MM-dd";
            this.dateTimePickerTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerTo.Location = new System.Drawing.Point(0, 22);
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.Size = new System.Drawing.Size(120, 20);
            this.dateTimePickerTo.TabIndex = 9;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panelDateFrom);
            this.panel2.Controls.Add(this.panelDateTo);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Location = new System.Drawing.Point(341, 14);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(142, 186);
            this.panel2.TabIndex = 10;
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Location = new System.Drawing.Point(12, 12);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(493, 223);
            this.panel3.TabIndex = 11;
            // 
            // panelDateTo
            // 
            this.panelDateTo.Controls.Add(this.labelDateTo);
            this.panelDateTo.Controls.Add(this.dateTimePickerTo);
            this.panelDateTo.Location = new System.Drawing.Point(9, 60);
            this.panelDateTo.Name = "panelDateTo";
            this.panelDateTo.Size = new System.Drawing.Size(123, 51);
            this.panelDateTo.TabIndex = 10;
            // 
            // labelDateTo
            // 
            this.labelDateTo.AutoSize = true;
            this.labelDateTo.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDateTo.Location = new System.Drawing.Point(33, 5);
            this.labelDateTo.Name = "labelDateTo";
            this.labelDateTo.Size = new System.Drawing.Size(52, 14);
            this.labelDateTo.TabIndex = 10;
            this.labelDateTo.Text = "DATA DO";
            // 
            // panelDateFrom
            // 
            this.panelDateFrom.Controls.Add(this.labelDateFrom);
            this.panelDateFrom.Controls.Add(this.dateTimePickerFrom);
            this.panelDateFrom.Location = new System.Drawing.Point(9, 3);
            this.panelDateFrom.Name = "panelDateFrom";
            this.panelDateFrom.Size = new System.Drawing.Size(123, 51);
            this.panelDateFrom.TabIndex = 11;
            // 
            // labelDateFrom
            // 
            this.labelDateFrom.AutoSize = true;
            this.labelDateFrom.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDateFrom.Location = new System.Drawing.Point(33, 5);
            this.labelDateFrom.Name = "labelDateFrom";
            this.labelDateFrom.Size = new System.Drawing.Size(52, 14);
            this.labelDateFrom.TabIndex = 10;
            this.labelDateFrom.Text = "DATA OD";
            // 
            // ReportView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WindowsFormsMVC.Properties.Resources.clouds_bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(511, 244);
            this.Controls.Add(this.panel3);
            this.Name = "ReportView";
            this.Text = "Raporty";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ReportsView_FormClosing);
            this.panelIncomes.ResumeLayout(false);
            this.panelTotalIncomes.ResumeLayout(false);
            this.panelTotalIncomes.PerformLayout();
            this.panelSale.ResumeLayout(false);
            this.panelSale.PerformLayout();
            this.panelOtherIncomes.ResumeLayout(false);
            this.panelOtherIncomes.PerformLayout();
            this.panelPurchases.ResumeLayout(false);
            this.panelPurchase.ResumeLayout(false);
            this.panelPurchase.PerformLayout();
            this.panelPurchaseCost.ResumeLayout(false);
            this.panelPurchaseCost.PerformLayout();
            this.panelOutcomes.ResumeLayout(false);
            this.panelTotalOutcomes.ResumeLayout(false);
            this.panelTotalOutcomes.PerformLayout();
            this.panelSalary.ResumeLayout(false);
            this.panelSalary.PerformLayout();
            this.panelOtherOutcomes.ResumeLayout(false);
            this.panelOtherOutcomes.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panelDateTo.ResumeLayout(false);
            this.panelDateTo.PerformLayout();
            this.panelDateFrom.ResumeLayout(false);
            this.panelDateFrom.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panelIncomes;
        private System.Windows.Forms.Panel panelTotalIncomes;
        private System.Windows.Forms.Label labelTotalIncomes;
        private System.Windows.Forms.TextBox textBoxTotalIncomesSum;
        private System.Windows.Forms.Panel panelSale;
        private System.Windows.Forms.Label labelSale;
        private System.Windows.Forms.TextBox textBoxSaleSum;
        private System.Windows.Forms.Panel panelOtherIncomes;
        private System.Windows.Forms.Label labelOtherIncomes;
        private System.Windows.Forms.TextBox textBoxOtherIncomesSum;
        private System.Windows.Forms.Panel panelPurchases;
        private System.Windows.Forms.Panel panelPurchase;
        private System.Windows.Forms.Label labelPurchase;
        private System.Windows.Forms.TextBox textBoxPurchaseSum;
        private System.Windows.Forms.Panel panelPurchaseCost;
        private System.Windows.Forms.Label labelPurchaseCost;
        private System.Windows.Forms.TextBox textBoxPurchaseCostSum;
        private System.Windows.Forms.Panel panelOutcomes;
        private System.Windows.Forms.Panel panelTotalOutcomes;
        private System.Windows.Forms.Label labelTotalOutcomes;
        private System.Windows.Forms.TextBox textBoxTotalOutcomesSum;
        private System.Windows.Forms.Panel panelSalary;
        private System.Windows.Forms.Label labelSalary;
        private System.Windows.Forms.TextBox textBoxSalarySum;
        private System.Windows.Forms.Panel panelOtherOutcomes;
        private System.Windows.Forms.Label labelOtherOutcomes;
        private System.Windows.Forms.TextBox textBoxOtherOutcomesSum;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.DateTimePicker dateTimePickerTo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panelDateFrom;
        private System.Windows.Forms.Label labelDateFrom;
        private System.Windows.Forms.Panel panelDateTo;
        private System.Windows.Forms.Label labelDateTo;
    }
}